package com.w4rlock.backpapermusic;

import android.annotation.SuppressLint;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.Connectivity;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

public class AbsActivity extends AppCompatActivity implements LoadingInterface {
    private static final String DEBUG_TAG = AbsActivity.class.toString();
    private View mSyncProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void showAlertIfOnMobile() {
        if(ApplicationProperties.ShowAlertIfOnMobileNetwork){
            if(!Connectivity.isConnected(getApplicationContext())){
                Utils.customAlert(AbsActivity.this,getString(R.string.alert_message_if_no_network));
                ApplicationProperties.ShowAlertIfOnMobileNetwork =false;
            }else if(!Connectivity.isConnectedWifi(getApplicationContext())){
                Utils.customAlert(AbsActivity.this,getString(R.string.alert_message_if_no_on_wifi));
                ApplicationProperties.ShowAlertIfOnMobileNetwork =false;
            }
        }
    }



    @Override
    public void onLoadingStarted() {
    }

    @Override
    public void onLoadingEnded() {
    }

    @Override
    public void onProgressUpdate(int percent) {

    }
}
