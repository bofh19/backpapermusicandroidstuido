package com.w4rlock.backpapermusic;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.w4rlock.backpapermusic.adapters.PlayerControlsFragment;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.L;

/**
 * Created by saikrishna on 18/06/14.
 */
public abstract class AbsPlayerActivity extends AbsActivity {
    public static final String DEBUG_TAG = AbsPlayerActivity.class.toString();
    protected Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.d(DEBUG_TAG, "on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.abs_player_activity);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (savedInstanceState == null) {
            Bundle playerArgs = new Bundle();
            playerArgs.putLong(ApplicationProperties.ServiceControlKey.KeyArgumentName, getServiceControlKey());
            Fragment playerFragment = new PlayerControlsFragment();
            playerFragment.setArguments(playerArgs);
            getSupportFragmentManager().beginTransaction().add(R.id.player_controls_container, playerFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.container, getMainFragment()).commit();
        }

    }

    public abstract Fragment getMainFragment();

    public abstract Long getServiceControlKey();

    @Override
    public void onPause() {
        L.d(DEBUG_TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        L.d(DEBUG_TAG, "onResume");
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
