package com.w4rlock.backpapermusic;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.w4rlock.backpapermusic.database.service.MusicDatabaseSyncerService;
import com.w4rlock.backpapermusic.fragments.MusicListFragment;
import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.L;

public class MusicActivity extends AbsPlayerActivity {
    private static final String DEBUG_TAG = MusicActivity.class.toString();
    MusicListFragment musicListFragment;
    ProgressDialog tempProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.d(DEBUG_TAG, "on create");
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
        tempProgressDialog = new ProgressDialog(this);
        L.d(DEBUG_TAG, "Calling Service");
        MusicDatabaseSyncerService musicDatabaseSyncerService = new MusicDatabaseSyncerService(getApplicationContext(),
                loadingInterface);
        musicDatabaseSyncerService.execute();
    }

    @Override
    public Fragment getMainFragment() {
        musicListFragment = new MusicListFragment();
        return musicListFragment;
    }

    @Override
    public Long getServiceControlKey() {
        return ApplicationProperties.ServiceControlKey.musicActivityPlayerControlServiceKey;
    }

    @Override
    public void onPause() {
        L.d(DEBUG_TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        L.d(DEBUG_TAG, "onResume");
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    final LoadingInterface loadingInterface = new LoadingInterface() {

        @Override
        public void onProgressUpdate(int percent) {

        }

        @Override
        public void onLoadingStarted() {
            if(tempProgressDialog.isShowing()){
                tempProgressDialog.dismiss();
            }
            tempProgressDialog.setMessage("Downloading Data from server. Please give it a sec if its the first time");
            tempProgressDialog.setCancelable(false);
            tempProgressDialog.show();
        }

        @Override
        public void onLoadingEnded() {
            if(tempProgressDialog.isShowing()){
                tempProgressDialog.dismiss();
            }
            if(musicListFragment != null)
                musicListFragment.forceLoadDataFromLoader();
        }
    };
}
