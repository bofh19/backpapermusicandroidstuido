package com.w4rlock.backpapermusic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.w4rlock.backpapermusic.fragments.MusicPlayListFragment;
import com.w4rlock.backpapermusic.util.ApplicationProperties;

public class MusicPlayListActivity extends AbsPlayerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
        }
    }

    @Override
    public Fragment getMainFragment() {
        return new MusicPlayListFragment();
    }

    @Override
    public Long getServiceControlKey() {
        return ApplicationProperties.ServiceControlKey.musicPlayListControlServiceKey;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent i = new Intent(this, MusicActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
