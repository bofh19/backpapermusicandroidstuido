package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;

import java.util.List;

public class MusicGridViewAdapter extends ArrayAdapter<Song> {
    List<Song> music;
    ImageLoader imageLoader;
    private LayoutInflater inflater;
    static int resourceId = R.layout.grid_view_each_album;
    private int imageViewWidth;
    private int imageViewHeight;
    public MusicGridViewAdapter(Context context, List<Song> music,int imageViewWidth) {
        super(context, resourceId, music);
        this.music = music;
        this.imageViewWidth = imageViewWidth;
        imageViewHeight = (int)(imageViewWidth);
        imageLoader = new ImageLoader(context,imageViewWidth,imageViewHeight);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        AlbumViewHolder albumViewHolder;
        if (row == null) {
            row = inflater.inflate(resourceId, parent, false);
            albumViewHolder = new AlbumViewHolder();
            albumViewHolder.songIcon = (ImageView) row.findViewById(R.id.album_icon);
            albumViewHolder.songLabel = (TextView) row.findViewById(R.id.album_label);
            albumViewHolder.songNewStatus = (ImageView) row.findViewById(R.id.album_new_status_icon);
            row.setTag(albumViewHolder);
        } else {
            albumViewHolder = (AlbumViewHolder) row.getTag();
        }
        Song m = this.music.get(position);
        albumViewHolder.songLabel.setText(m.getDirname());

        albumViewHolder.songIcon.setAdjustViewBounds(true);
        albumViewHolder.songIcon.setMaxHeight(imageViewHeight);
        albumViewHolder.songIcon.setMinimumHeight(imageViewHeight);
        albumViewHolder.songIcon.setMaxWidth(imageViewWidth);
        albumViewHolder.songIcon.setMinimumWidth(imageViewWidth);

        imageLoader.DisplayImage(m.getThumbnail(), albumViewHolder.songIcon);
        if(m.isIs_new())
            albumViewHolder.songNewStatus.setVisibility(View.VISIBLE);
        else
            albumViewHolder.songNewStatus.setVisibility(View.GONE);
        return row;
    }

    static class AlbumViewHolder {
        public ImageView songIcon;
        public TextView songLabel;
        public ImageView songNewStatus;
    }
}
