package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;

import java.util.ArrayList;

public class MusicListDisplayAdapter extends BaseExpandableListAdapter {
    ImageLoader imageLoader;
    private LayoutInflater inflater;
    private ArrayList<Song> music;
    private Context context;

    public MusicListDisplayAdapter(Context context, ArrayList<Song> music) {
        this.music = music;
        this.context = context;
        imageLoader = new ImageLoader(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class AlbumViewHolder {
        public ImageView songIcon;
        public TextView songLabel;
    }

    static class SongViewHolder {
        public TextView songLabel;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // return
        // this.music.get(groupPosition).getFiles().getMp3().get(childPosition);
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        // final MP3 mp3 = (MP3)getChild(groupPosition, childPosition);
        // TextView textView;
        // if(convertView == null){
        // textView = new TextView(context);
        // }else{
        // textView = (TextView)convertView;
        // }
        // textView.setText(mp3.getName());
        // return textView;
        View row = convertView;
        SongViewHolder songViewHolder;
        if (row == null) {
            row = inflater.inflate(R.layout.each_song, parent, false);
            songViewHolder = new SongViewHolder();
            songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
            row.setTag(songViewHolder);
        } else {
            songViewHolder = (SongViewHolder) row.getTag();
        }
        Song m = this.music.get(groupPosition);
        songViewHolder.songLabel.setText(m.getSongName(childPosition));
        return row;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return this.music.get(groupPosition).getFiles().getMp3().size();
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.music.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.music.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View row = convertView;
        AlbumViewHolder albumViewHolder;
        if (row == null) {
            row = inflater.inflate(R.layout.each_album, parent, false);
            albumViewHolder = new AlbumViewHolder();
            albumViewHolder.songIcon = (ImageView) row.findViewById(R.id.album_icon);
            albumViewHolder.songLabel = (TextView) row.findViewById(R.id.album_label);
            row.setTag(albumViewHolder);
        } else {
            albumViewHolder = (AlbumViewHolder) row.getTag();
        }
        Song m = this.music.get(groupPosition);
        albumViewHolder.songLabel.setText(m.getDirname());
        imageLoader.DisplayImage(m.getBaseUrl() + m.getThumbnail(), albumViewHolder.songIcon);
        return row;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }

}
