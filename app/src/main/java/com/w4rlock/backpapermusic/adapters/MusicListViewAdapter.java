package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;

import java.util.List;

public class MusicListViewAdapter extends ArrayAdapter<Song> {
    List<Song> music;
    ImageLoader imageLoader;
    private LayoutInflater inflater;
    static int resourceId = R.layout.each_album;

    public MusicListViewAdapter(Context context, List<Song> music) {
        super(context, resourceId, music);
        this.music = music;
        imageLoader = new ImageLoader(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        AlbumViewHolder albumViewHolder;
        if (row == null) {
            row = inflater.inflate(R.layout.each_album, parent, false);
            albumViewHolder = new AlbumViewHolder();
            albumViewHolder.songIcon = (ImageView) row.findViewById(R.id.album_icon);
            albumViewHolder.songLabel = (TextView) row.findViewById(R.id.album_label);
            row.setTag(albumViewHolder);
        } else {
            albumViewHolder = (AlbumViewHolder) row.getTag();
        }
        Song m = this.music.get(position);
        albumViewHolder.songLabel.setText(m.getDirname());
        imageLoader.DisplayImage(m.getThumbnail(), albumViewHolder.songIcon);
        return row;
    }

    static class AlbumViewHolder {
        public ImageView songIcon;
        public TextView songLabel;
    }
}
