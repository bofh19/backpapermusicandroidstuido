package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.loaders.LoadSongsList;
import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;
import com.w4rlock.backpapermusic.views.TextViewPlus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by saikrishnak on 11/7/14.
 */
public class MusicRecyclerAdapter extends RecyclerView.Adapter<MusicRecyclerAdapter.AlbumViewHolder> {
    public static String DEBUG_TAG = MusicRecyclerAdapter.class.toString();
    public static class AlbumViewHolder extends RecyclerView.ViewHolder{
        public ImageView songIcon;
        public TextView songLabel;
        public View buttonLayout;
        public LinearLayout mSongsListView;
        public AlbumViewHolder(View rowView) {
            super(rowView);
            this.songIcon = (ImageView) rowView.findViewById(R.id.album_icon);
            this.songLabel = (TextView) rowView.findViewById(R.id.album_label);
            buttonLayout = (View) rowView.findViewById(R.id.button_layout);
            mSongsListView = (LinearLayout) rowView.findViewById(R.id.songs_full_list);
            buttonLayout.setVisibility(View.GONE);
        }

    }

    public interface RecyclerViewSongSelected{
        public void onSongSelected(SongItem songItem);
        public void onSongsSelected(List<SongItem> songItems);
    }

    List<Song> music;
    static int resourceId = R.layout.each_album_recycler;
    private static int songItemResourceId = R.layout.recycler_view_each_song;
    ImageLoader imageLoader;
    private Context context;
    private LayoutInflater inflater;
    private RecyclerViewSongSelected callback;
    public MusicRecyclerAdapter(List<Song> music,Context context,RecyclerViewSongSelected callback) {
        this.music = music;
        imageLoader = new ImageLoader(context);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.callback = callback;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
        AlbumViewHolder vh = new AlbumViewHolder(v);
        return vh;
    }
    int selectedRow = -1;
    @Override
    public void onBindViewHolder(final AlbumViewHolder albumViewHolder, final int position) {
        final Song m = this.music.get(position);
        albumViewHolder.songLabel.setText(m.getDirname());
        imageLoader.DisplayImage(m.getThumbnail(), albumViewHolder.songIcon);
        if(position == selectedRow){
//            albumViewHolder.buttonLayout.setVisibility(View.VISIBLE);
//            albumViewHolder.mSongsListView.setVisibility(View.VISIBLE);
        }else{
//            albumViewHolder.buttonLayout.setVisibility(View.GONE);
            albumViewHolder.mSongsListView.removeAllViews();
//            albumViewHolder.mSongsListView.setVisibility(View.GONE);
        }
        albumViewHolder.songIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedRow != position) {
                    int previous_pos = selectedRow;
                    selectedRow = position;
                    notifyItemChanged(selectedRow);
                    if(albumViewHolder.mSongsListView.getChildCount() <= 0){
                        loadSongItemsFromSong(m,albumViewHolder.mSongsListView);
                    }
                    if(previous_pos != -1){
                        notifyItemChanged(previous_pos);
                    }
                }else{
                    selectedRow = -1;
                    notifyItemChanged(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if(music!=null)
            return music.size();
        else
            return 0;
    }

    private void loadSongItemsFromSong(Song song,final LinearLayout mSongsListView) {
        LoadingInterface loadingInterface = new LoadingInterface() {
            @Override
            public void onLoadingStarted() {
                View row = inflater.inflate(songItemResourceId, mSongsListView,false);
                mSongsListView.addView(row);
                TextView songLine1 = (TextView) row.findViewById(R.id.song_line_1);
                TextView songLine2 = (TextView) row.findViewById(R.id.song_line_2);
                TextView songLine3 = (TextView) row.findViewById(R.id.song_line_3);
                songLine1.setText("Loading...");
                songLine2.setText(context.getString(R.string.loading));
                songLine3.setText(context.getString(R.string.loading));

            }

            @Override
            public void onLoadingEnded() {

            }

            @Override
            public void onProgressUpdate(int percent) {

            }
        };
        final LoadSongsList.LoadSongsListCallback callback = new LoadSongsList.LoadSongsListCallback() {

            @Override
            public void onLoadingFinished(ArrayList<Song> songs) {
                mSongsListView.removeAllViews();
                for (Song song : songs) {
                    L.d(DEBUG_TAG, song.getName() + "/" + song.getDirname());
                    final SongItem songItem = new SongItem(song);
                    View row = inflater.inflate(songItemResourceId, mSongsListView,false);
                    mSongsListView.addView(row);
                    TextView songLine1 = (TextView) row.findViewById(R.id.song_line_1);
                    TextView songLine2 = (TextView) row.findViewById(R.id.song_line_2);
                    TextView songLine3 = (TextView) row.findViewById(R.id.song_line_3);
//                    LinearLayout songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
                    songLine1.setText(songItem.getTitle());
                    songLine2.setText(songItem.getAlbum() + "/" + songItem.getArtist());
                    songLine3.setText(songItem.getReadableDuration() + " " + songItem.getReadableSize());
                    row.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(DEBUG_TAG, "clicked on this song");
                            MusicRecyclerAdapter.this.callback.onSongSelected(songItem);
                        }
                    });
                }
                // show items here
            }
        };
        LoadSongsList loadSongsList = new LoadSongsList(context, loadingInterface, callback);
        loadSongsList.execute(song);
    }

}
