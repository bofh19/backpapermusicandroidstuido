package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.w4rlock.backpapermusic.AbsPlayerActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.controllers.playlistToSong.PlayListToSongDisplayer;
import com.w4rlock.backpapermusic.controllers.playlistToSong.SongViewHolder;
import com.w4rlock.backpapermusic.models.PlayListItem;

import java.util.ArrayList;

public class PlayListDisplayAdapter extends ArrayAdapter<PlayListItem> {
    private static final String DEBUG_TAG = PlayListDisplayAdapter.class.toString();
    private LayoutInflater inflater;
    PlayListToSongDisplayer playListToSongDisplayer;
    PlayListDisplayAdapterCallback callback;

    public boolean isDisplayRemoveButton() {
        return displayRemoveButton;
    }

    public void setDisplayRemoveButton(boolean displayRemoveButton) {
        this.displayRemoveButton = displayRemoveButton;
    }

    private boolean displayRemoveButton = false;

    public PlayListItem getCurrentPlayListItem() {
        return currentPlayListItem;
    }

    public void setCurrentPlayListItem(PlayListItem currentPlayListItem) {
        this.currentPlayListItem = currentPlayListItem;
    }

    private PlayListItem currentPlayListItem;

    public PlayListDisplayAdapter(Context context, ArrayList<PlayListItem> playListItems, PlayListItem currentPlayingItem, PlayListDisplayAdapterCallback callback) {
        super(context, R.layout.playlist_each_song, playListItems);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        playListToSongDisplayer = new PlayListToSongDisplayer(context);
        this.currentPlayListItem = currentPlayingItem;
        this.callback = callback;
    }

    @Override
    public void add(PlayListItem object) {
        super.add(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View row;
        SongViewHolder songViewHolder;
        if (convertView == null) {
            row = inflater.inflate(R.layout.playlist_each_song, parent, false);
            songViewHolder = new SongViewHolder();
            songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
            songViewHolder.songIcon = (ImageView) row.findViewById(R.id.song_icon);
            songViewHolder.songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
            songViewHolder.songDeleteIcon = (ImageView) row.findViewById(R.id.song_delete_icon);
            songViewHolder.songStreamType = (TextView) row.findViewById(R.id.song_stream_type);
            songViewHolder.needInflate = false;
            row.setTag(songViewHolder);
        } else if (((SongViewHolder) convertView.getTag()).needInflate) {
            row = inflater.inflate(R.layout.playlist_each_song, parent, false);
            songViewHolder = new SongViewHolder();
            songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
            songViewHolder.songIcon = (ImageView) row.findViewById(R.id.song_icon);
            songViewHolder.songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
            songViewHolder.songDeleteIcon = (ImageView) row.findViewById(R.id.song_delete_icon);
            songViewHolder.songStreamType = (TextView) row.findViewById(R.id.song_stream_type);
            songViewHolder.needInflate = false;
            row.setTag(songViewHolder);
        } else {
            row = convertView;
            songViewHolder = (SongViewHolder) row.getTag();
        }
        PlayListItem playListItem = getItem(position);


        ((AbsPlayerActivity)getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
        playListToSongDisplayer.DisplayPlayListDetails(playListItem.hashKey.longValue(), songViewHolder);

        if (currentPlayListItem != null && currentPlayListItem.equals(playListItem)) {
            Drawable img = getContext().getResources().getDrawable(R.drawable.visualizations_scaled);
            img.setBounds(0, 0, 60, 60);
            songViewHolder.songLabel.setCompoundDrawables(null, null, img, null);
        } else {
            songViewHolder.songLabel.setCompoundDrawables(null, null, null, null);
        }
        if (isDisplayRemoveButton()) {
            songViewHolder.songDeleteIcon.setVisibility(View.VISIBLE);
        } else {
            songViewHolder.songDeleteIcon.setVisibility(View.GONE);
        }
        songViewHolder.songDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onDelete(row, position);
            }
        });
        return row;
    }

    public static interface PlayListDisplayAdapterCallback {
        public void onDelete(View view, int position);
    }
}
