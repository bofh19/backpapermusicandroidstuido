package com.w4rlock.backpapermusic.adapters;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.MusicPlayerActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.binder.MusicServiceBinder;
import com.w4rlock.backpapermusic.service.binder.MusicServiceCallbackInterface;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

/**
 * Created by saikrishna on 18/06/14.
 */
public class PlayerControlsFragment extends Fragment implements MusicServiceCallbackInterface {
    private static final String DEBUG_TAG = PlayerControlsFragment.class.toString();
    private Long mServiceControlKey;
    boolean mBound = false;
    MusicService mMusicService;
    private TextView mMediaTime;
    private MediaPlayer mPlayer;
    private SongItem mSongItem;
    private ImageView mSongThumb;
    private TextView mMediaPercent;
    private TextView mSongDetails;
    private ViewGroup mainLayout;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        L.d(DEBUG_TAG, "onResume");
        super.onResume();
        attachToService();
    }

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isMusicServiceRunning(getActivity()))
        // binding only if music
        // service is already
        // running
        {
            Intent intent = new Intent(getActivity(), MusicService.class);
            getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        } else
        // we set mSongItem to null and call
        // write song details
        // to set default values
        {
            mSongItem = null;
        }
    }

    @Override
    public void onPause() {
        L.d(DEBUG_TAG, "onPause");
        super.onPause();
        detachFromService();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void detachFromService() {
        // Unbind from the service
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
        handler.removeCallbacks(onEverySecond);
        detachMusicPlayerCallbacks();
        mMusicService = null;
    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            detachMusicPlayerCallbacks();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            MusicServiceBinder binder = (MusicServiceBinder) service;
            mMusicService = binder.getService();
            mBound = true;
            attachMusicPlayerCallbacks();
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.mini_player_fragment, container, false);
        mServiceControlKey = getArguments().getLong(ApplicationProperties.ServiceControlKey.KeyArgumentName, -1L);
        if (mServiceControlKey.equals(-1L)) {
            throw new IllegalStateException("Must Provide " + ApplicationProperties.ServiceControlKey.KeyArgumentName + " to attach");
        }
        mMediaTime = (TextView) rootView.findViewById(R.id.mini_player_media_time);
        mSongThumb = (ImageView) rootView.findViewById(R.id.mini_player_thumbnail);
        mMediaPercent = (TextView) rootView.findViewById(R.id.mini_player_media_percent);
        mainLayout = (ViewGroup) rootView.findViewById(R.id.mini_player_main_layout);
        mSongDetails = (TextView) rootView.findViewById(R.id.mini_player_song_details);
        (rootView.findViewById(R.id.mini_player_pause_button)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_PAUSE);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });

        mSongThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent playerIntent = new Intent(getActivity(), MusicPlayerActivity.class);
                playerIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().startActivity(playerIntent);
            }
        });

        (rootView.findViewById(R.id.mini_player_next_button)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_SKIP);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        (rootView.findViewById(R.id.mini_player_pause_button)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_REWIND);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        (rootView.findViewById(R.id.mini_player_play_button)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                detachFromService();
                Intent i = new Intent(MusicService.ACTION_PLAY);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
                attachToService();

            }
        });

        (rootView.findViewById(R.id.mini_player_stop_button)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_STOP);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });

        return rootView;
    }


    protected void detachMusicPlayerCallbacks() {
        if (mMusicService != null)
            mMusicService.unregisterCallbackListener(mServiceControlKey, PlayerControlsFragment.this);
        mPlayer = null;
    }

    protected void attachMusicPlayerCallbacks() {
        mMusicService.unregisterCallbackListener(mServiceControlKey, PlayerControlsFragment.this);
        mMusicService.registerCallbackListener(mServiceControlKey, PlayerControlsFragment.this);
    }

    private void writeSongDetails(SongItem songItem, boolean onPreperaing) {
        mSongItem = songItem;
        writeSongDetails(onPreperaing);
    }

    private void writeSongDetails(boolean onPreparing) {
        if (mSongItem != null) {
            mSongDetails.setText(mSongItem.getTitle() + " - " + mSongItem.getAlbum() + " - " + mSongItem.getArtist()
                    + (onPreparing ? " (Loading)" : " (Playing)"));
        } else {
            mSongDetails.setText(getActivity().getString(R.string.playing_nothing));
            mMediaTime.setText(getActivity().getString(R.string.time_duration_as_zeros));
            mSongThumb.setImageResource(R.drawable.dummy_album_art);
        }
    }


    @Override
    public void onSongChanged(SongItem songItem) {

    }

    @Override
    public void onStateChanged(MusicService.State state) {

    }

    @Override
    public void onPause(SongItem songItem) {

    }

    @Override
    public void onPlay(SongItem songItem, MediaPlayer mPlayer) {
        L.d(DEBUG_TAG, "onPlay");
        mSongItem = songItem;
        this.mPlayer = mPlayer;
        handler.postDelayed(onEverySecond, 1000);
        mMediaPercent.setText("");
        writeSongDetails(false);
    }

    @Override
    public void onMediaPlayerStop() {
        L.d(DEBUG_TAG, "onMediaPlayerStop");
        mSongItem = null;
        handler.removeCallbacks(onEverySecond);
        writeSongDetails(false);
    }

    @Override
    public void onSongCompletion() {

    }

    @Override
    public void onPreparing(SongItem songItem) {
        L.d(DEBUG_TAG, "onPreparing");
        writeSongDetails(songItem, true);
    }

    @Override
    public void onPrepared(MediaPlayer mPlayer) {

    }

    @Override
    public void onError(MediaPlayer mp, int what, int extra) {

    }

    @Override
    public void onBufferUpdate(MediaPlayer mp, int percent) {
        L.d(DEBUG_TAG, "onBufferUpdate " + percent);
        mMediaPercent.setText(" - " + percent);
    }

    @Override
    public void onMediaPlayerReleased() {

    }

    @Override
    public void onMediaPlayerCreated() {

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onMostCommonColorAcquired(String hexColor) {
        L.d(DEBUG_TAG, "onMostCommonColorAcquired " + hexColor);
//        if (hexColor != null) {
//            try {
//                mainLayout.setBackgroundColor(Color.parseColor(hexColor));
//                if (Utils.isOverHoneyComb()) {
//                    ActionBar bar = getActivity().getActionBar();
//                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(hexColor)));
//                } else {
//                    android.support.v7.app.ActionBar bar = ((ActionBarActivity) getActivity()).getSupportActionBar();
//                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(hexColor)));
//                }
//            } catch (Exception e) {
//                L.d(DEBUG_TAG, "unable to set most common color: " + e.toString());
//            }
//        }
    }

    @Override
    public void onThumbnailDrawableAcquired(Bitmap result) {
        L.d(DEBUG_TAG, "onThumbnailDrawableAcquired ");
        if (!isAdded()) {
            return;
        }
        if (result != null) {
            mSongThumb.setImageBitmap(result);
        } else {
            L.d(DEBUG_TAG, "onThumbnailDrawableAcquired Result is Null");
            mSongThumb.setImageResource(R.drawable.dummy_album_art);
        }
    }

    // updating seekbar and timers
    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            handler.removeCallbacks(onEverySecond);
            try {
                if (mPlayer != null && mPlayer.isPlaying()) {
                    duration = mPlayer.getDuration();
                    updateTime();
                }
            } catch (Exception e) {
                L.d(DEBUG_TAG, "" + e.toString());
            }
            handler.postDelayed(onEverySecond, 1000);
        }
    };

    private int current = 0;
    private int duration = 0;
    private final Handler handler = new Handler();

    private void updateTime() {
        current = mPlayer.getCurrentPosition();
//        L.d(DEBUG_TAG, "duration - " + duration + " current- " + current);
        int dSeconds = (int) (duration / 1000) % 60;
        int dMinutes = (int) ((duration / (1000 * 60)) % 60);
        int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

        int cSeconds = (int) (current / 1000) % 60;
        int cMinutes = (int) ((current / (1000 * 60)) % 60);
        int cHours = (int) ((current / (1000 * 60 * 60)) % 24);

        if (dHours == 0) {
            mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
        } else {
            mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours,
                    dMinutes, dSeconds));
        }
    }
}
