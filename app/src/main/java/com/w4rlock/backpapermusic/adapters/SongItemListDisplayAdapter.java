package com.w4rlock.backpapermusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;

import java.util.ArrayList;

public class SongItemListDisplayAdapter extends ArrayAdapter<SongItem> {
    private LayoutInflater inflater;
    private static int resourceId = R.layout.song_dialog_each_song;
    public SongItemListDisplayAdapter(Context context, ArrayList<SongItem> songs) {
        super(context, resourceId, songs);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SongViewHolder songViewHolder;
        if (row == null) {
            row = inflater.inflate(resourceId, parent, false);
            songViewHolder = new SongViewHolder();
            songViewHolder.songLabel = (TextView) row.findViewById(R.id.song_label);
            songViewHolder.songLableParent = (LinearLayout) row.findViewById(R.id.song_lable_parent_ll);
            row.setTag(songViewHolder);
        } else {
            songViewHolder = (SongViewHolder) row.getTag();
        }
        SongItem songItem = getItem(position);
        songViewHolder.songLabel.setText(songItem.getSongDialogListDisplayString());
        if (songItem.isSelected) {
            songViewHolder.songLableParent.setBackgroundColor(getContext().getResources().getColor(
                    R.color.holo_blue_light));
        } else {
            songViewHolder.songLableParent.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        }
        return row;
    }

    static class SongViewHolder {
        public LinearLayout songLableParent;
        public TextView songLabel;
    }
}
