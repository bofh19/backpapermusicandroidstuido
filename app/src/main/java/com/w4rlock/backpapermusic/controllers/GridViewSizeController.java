package com.w4rlock.backpapermusic.controllers;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.widget.GridView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

/**
 * Created by saikrishna on 11/06/14.
 */
public class GridViewSizeController {
    private static final Class clazz = GridViewSizeController.class;
    /*
    sets the column width of a grid view based on grid padding and no of columns
     */
    public static int fixGridViewColumnWidth(Context context, GridView gridView) {
        Resources r = context.getResources();
        int noOfColumns = r.getInteger(R.integer.gridview_no_of_cols);
        int gridPadding = r.getInteger(R.integer.gridview_padding);
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, gridPadding, r.getDisplayMetrics());

        int columnWidth = (int) ((Utils.getScreenWidth(context) - (noOfColumns + 1) * padding) / noOfColumns);
        L.d(clazz,"calculated columnWidth: "+columnWidth);
        gridView.setNumColumns(noOfColumns);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
        return columnWidth;
    }
}
