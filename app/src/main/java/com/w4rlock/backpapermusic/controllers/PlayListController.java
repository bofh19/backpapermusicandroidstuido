package com.w4rlock.backpapermusic.controllers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.PlayListItem;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;

import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class PlayListController {
    public static final String DEBUG_TAG = PlayListController.class.toString();

    public static SongItem getNextSongItem(Context context) {
        L.d(DEBUG_TAG, "getNextSong called");
        PlayListItem savedPlayListItem = PlayListItem.getPlayingItemHashKey(context);
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        Cursor playListItems = cupboard().withDatabase(db).query(PlayListItem.class).getCursor();
        L.d(DEBUG_TAG, "found hash key called " + savedPlayListItem);
        QueryResultIterable<PlayListItem> itr = cupboard().withCursor(playListItems).iterate(PlayListItem.class);
        boolean nextOneIsTheOne = false;
        boolean first = true;
        PlayListItem firstItem = null;
        try {
            for (PlayListItem playListItem : itr) {
                L.d(DEBUG_TAG, "playListItem.hashKey " + playListItem + " // hashKey: " + savedPlayListItem);
                if (first) {
                    first = false;
                    firstItem = playListItem;
                }
                if (nextOneIsTheOne) {
                    L.d(DEBUG_TAG, "this is the on that's going to play next " + playListItem.hashKey);
                    Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + playListItem.hashKey).get();
                    playingThisSong(playListItem, context);
                    return new SongItem(song);
                }
                if (playListItem.equals(savedPlayListItem)) {
                    L.d(DEBUG_TAG, "found current playing one " + savedPlayListItem);
                    nextOneIsTheOne = true;
                }
            }
            L.d(DEBUG_TAG, "found no playing song . so returning first playListItem which is " + firstItem);
            if (firstItem != null) {
                Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + firstItem.hashKey).get();
                playingThisSong(firstItem, context);
                return new SongItem(song);
            }
        } catch (Exception e) {
            L.d(DEBUG_TAG, "error while trying to get next song " + e.toString());
            e.printStackTrace();
        } finally {
            db.close();
            itr.close();
        }
        return null;
    }

    public static SongItem getPreviousSongItem(Context context) {
        L.d(DEBUG_TAG, "getPreviousSongItem called");
        PlayListItem savedPlayListItem = PlayListItem.getPlayingItemHashKey(context);
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        Cursor playListItems = cupboard().withDatabase(db).query(PlayListItem.class).getCursor();
        L.d(DEBUG_TAG, "found hash key called " + savedPlayListItem);
        QueryResultIterable<PlayListItem> itr = cupboard().withCursor(playListItems).iterate(PlayListItem.class);
        boolean previousOneIsTheOne = false;
        boolean first = true;
        PlayListItem firstItem = null;
        PlayListItem previousItem = null;
        try {
            for (PlayListItem playListItem : itr) {
                L.d(DEBUG_TAG, "playListItem.hashKey " + playListItem + " // hashKey: " + savedPlayListItem);
                if (first) {
                    first = false;
                    firstItem = playListItem;
                }
                if (previousOneIsTheOne) {
                    L.d(DEBUG_TAG, "previousOneIsTheOne is " + previousOneIsTheOne + " this is the on that's going to play next " + playListItem);
                    Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + previousItem.hashKey).get();
                    playingThisSong(playListItem, context);
                    return new SongItem(song);
                }
                if (playListItem.equals(savedPlayListItem)) {
                    L.d(DEBUG_TAG, "found current playing one " + savedPlayListItem);
                    previousOneIsTheOne = true;
                } else {
                    previousItem = playListItem;
                }
            }
            L.d(DEBUG_TAG, "found no playing song . so returning first playListItem which is " + firstItem);
            if (firstItem != null) {
                Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + firstItem.hashKey).get();
                playingThisSong(firstItem, context);
                return new SongItem(song);
            }
        } catch (Exception e) {
            L.d(DEBUG_TAG, "error while trying to get next song " + e.toString());
            e.printStackTrace();
        } finally {
            db.close();
            itr.close();
        }
        return null;
    }

    public static void playingThisSong(PlayListItem playListItem, Context context) {
        L.d(DEBUG_TAG, "saving hash key " + playListItem);
        PlayListItem.saveToPreferences(context, playListItem);
    }

    public static ArrayList<PlayListItem> getFullPlayList(Context context) {
        ArrayList<PlayListItem> playListItemsResult = new ArrayList<PlayListItem>();
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        Cursor playListItems = cupboard().withDatabase(db).query(PlayListItem.class).getCursor();
        QueryResultIterable<PlayListItem> itr = cupboard().withCursor(playListItems).iterate(PlayListItem.class);
        try {
            for (PlayListItem playListItem : itr) {
                playListItemsResult.add(playListItem);
            }
        } catch (Exception e) {
            L.d(DEBUG_TAG, "error while trying to get all songs " + e.toString());
            e.printStackTrace();
        } finally {
            db.close();
            itr.close();
        }
        return playListItemsResult;
    }

    public static void addToPlayList(ArrayList<SongItem> songItems, Context context) {
        ArrayList<PlayListItem> playListItems = new ArrayList<>();
        for (SongItem songItem : songItems) {
            PlayListItem playListItem = new PlayListItem();
            playListItem.hashKey = songItem.getSong().getHash_key();
            playListItems.add(playListItem);
        }
        SQLiteDatabase db = MusicDb.getWriteDatabase(context);
        try {
            cupboard().withDatabase(db).put(playListItems);
        } finally {
            db.close();
        }
    }

    public static PlayListItem getCurrentPlayingItem(Context context) {
        return PlayListItem.getPlayingItemHashKey(context);
    }

    public static SongItem getSongItemFromPlayListItem(PlayListItem playListItem, Context context) {
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + playListItem.hashKey).get();
        return new SongItem(song);
    }

    public static boolean removeFromPlayList(PlayListItem playListItem,Context context){
        SQLiteDatabase db = MusicDb.getWriteDatabase(context);
        try {
            cupboard().withDatabase(db).delete(PlayListItem.class,playListItem._id);
            return true;
        }catch (Exception e){
            L.d(DEBUG_TAG,"Error while deleting "+e.toString());
        }finally {
            db.close();
        }
        return false;
    }
}
