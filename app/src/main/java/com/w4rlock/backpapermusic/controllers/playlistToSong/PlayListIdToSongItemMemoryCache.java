package com.w4rlock.backpapermusic.controllers.playlistToSong;

import android.util.Log;

import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Memory;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by saikrishnak on 15/06/14.
 */
public class PlayListIdToSongItemMemoryCache {

    private static final String TAG = "MemoryCache";
    private Map<Long, SongItem> cache = Collections.synchronizedMap(
            new LinkedHashMap<Long, SongItem>(10, 1.5f, true));//Last argument true for LRU ordering
    private long size = 0;//current allocated size
    private long limit = 1000000;//max memory in bytes

    public PlayListIdToSongItemMemoryCache() {
        //use 25% of available heap size
        setLimit(Runtime.getRuntime().maxMemory() / 4);
    }

    public void setLimit(long new_limit) {
        limit = new_limit;
        Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
    }

    public SongItem get(Long id) {
        try {
            if (!cache.containsKey(id))
                return null;
            //NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78
            return cache.get(id);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void put(Long id, SongItem songItem) {
        try {
            if (cache.containsKey(id))
                size -= getSizeInBytes(cache.get(id));
            cache.put(id, songItem);
            size += getSizeInBytes(songItem);
            checkSize();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void checkSize() {
        Log.i(TAG, "cache size=" + size + " length=" + cache.size());
        if (size > limit) {
            Iterator<Map.Entry<Long, SongItem>> iter = cache.entrySet().iterator();//least recently accessed item will be the first one iterated
            while (iter.hasNext()) {
                Map.Entry<Long, SongItem> entry = iter.next();
                size -= getSizeInBytes(entry.getValue());
                iter.remove();
                if (size <= limit)
                    break;
            }
            Log.i(TAG, "Clean cache. New size " + cache.size());
        }
    }

    public void clear() {
        try {
            //NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78
            cache.clear();
            size = 0;
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    long getSizeInBytes(SongItem songItem) {
        if (songItem == null)
            return 0;
        try {
            return Memory.sizeOf(songItem);
        } catch (IOException e) {
            L.d(TAG, "error while fetching object size: " + e.toString());
            return 0;
        }
    }
}
