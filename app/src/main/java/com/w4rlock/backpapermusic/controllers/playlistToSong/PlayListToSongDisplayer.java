package com.w4rlock.backpapermusic.controllers.playlistToSong;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Handler;
import android.os.Looper;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by saikrishnak on 15/06/14.
 */
public class PlayListToSongDisplayer {
    private static final String DEBUG_TAG = PlayListToSongDisplayer.class.toString();
    PlayListIdToSongItemMemoryCache memoryCache = new PlayListIdToSongItemMemoryCache();
    private Map<SongViewHolder, Long> songViews = Collections.synchronizedMap(new WeakHashMap<SongViewHolder, Long>());
    ExecutorService executorService;

    Context context;
    String stub_string;
    ImageLoader imageLoader;

    public PlayListToSongDisplayer(Context context) {
        init(context);
    }

    private void init(Context context) {
        executorService = Executors.newFixedThreadPool(5);
        this.context = context;
        stub_string = context.getResources().getString(R.string.loading);
        imageLoader = new ImageLoader(context);
    }

    public void DisplayPlayListDetails(Long playListId, SongViewHolder songViewHolder) {
        songViews.put(songViewHolder, playListId);
        SongItem songItem = memoryCache.get(playListId);
        if (songItem != null)
            setSongItem(songViewHolder, songItem, null);
        else {
            queuePlayListItemToLoad(playListId, songViewHolder);
            songViewHolder.songLabel.setText(stub_string);
        }
    }

    public void setSongItem(final SongViewHolder songViewHolder, final SongItem songItem, Handler handler) {
        setSongItemThumbOnly(songViewHolder, songItem, handler);
        songViewHolder.songLabel.setText(songItem.getPlayListDisplayString());
        songViewHolder.songStreamType.setText(context.getString(songItem.getStreamType()));
    }

    public void setSongItemThumbOnly(final SongViewHolder songViewHolder, final SongItem songItem, Handler handler) {
        if(handler==null){
            handler = new Handler(Looper.myLooper());
        }
        executorService.submit(new SongThumbnailLoader(songViewHolder, songItem, handler));
    }

    private void queuePlayListItemToLoad(Long playListId, SongViewHolder songViewHolder) {
        Handler handler = new Handler(Looper.getMainLooper());
        SongItemToLoad tl = new SongItemToLoad(playListId, songViewHolder, handler);
        executorService.submit(new SongItemsLoader(tl));
    }

    public SongItem getSongItem(Long playListItemId) {
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + playListItemId).get();
        return new SongItem(song);
    }

    //Task for the queue
    private class SongItemToLoad {
        public Long playListId;
        public SongViewHolder songViewHolder;
        public Handler handler;

        public SongItemToLoad(Long id, SongViewHolder songViewHolder, Handler handler) {
            playListId = id;
            this.songViewHolder = songViewHolder;
            this.handler = handler;
        }
    }

    class SongThumbnailLoader implements Runnable{
        SongItem songItem;
        SongViewHolder songViewHolder;
        Handler handler;

        SongThumbnailLoader(SongViewHolder songViewHolder, SongItem songItem, Handler handler){
            this.songItem = songItem;
            this.songViewHolder = songViewHolder;
            this.handler = handler;
        }

        @Override
        public void run(){
            boolean loadFromThumb = true;
            if(songItem.isLocalCopyAvailable()){
                android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(songItem.getSongPath());
                final byte [] data = mmr.getEmbeddedPicture();
                if(data != null)
                {
                    loadFromThumb = false;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            songViewHolder.songIcon.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
                        }
                    });
                }
            }
            if(loadFromThumb)
                imageLoader.DisplayImage(songItem.getThumbUrl(), songViewHolder.songIcon);
        }
    }

    class SongItemsLoader implements Runnable {
        SongItemToLoad songItemToLoad;
        SongItemsLoader(SongItemToLoad songItemToLoad) {
            this.songItemToLoad = songItemToLoad;
        }

        @Override
        public void run() {
            SongItem songItem = null;
            try {
                songItem = getSongItem(songItemToLoad.playListId);
            } catch (Throwable th) {
                L.d(DEBUG_TAG, "Error in songitemsloader: " + th.toString());
                th.printStackTrace();
            }
            final SongItem songItem1 = songItem;
            songItemToLoad.handler.post(new Runnable() {
                @Override
                public void run() {
                    if(songItem1 != null){
                        memoryCache.put(songItemToLoad.playListId, songItem1);
                        setSongItem(songItemToLoad.songViewHolder, songItem1, songItemToLoad.handler);
                    }else{
                        songItemToLoad.songViewHolder.songLabel.setText(context.getString(R.string.unknown_error));
                    }
                }
            });
        }
    }

    public void clearCache() {
        memoryCache.clear();
    }
}
