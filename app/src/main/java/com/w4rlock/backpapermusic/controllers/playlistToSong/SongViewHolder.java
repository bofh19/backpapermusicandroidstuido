package com.w4rlock.backpapermusic.controllers.playlistToSong;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by saikrishnak on 15/06/14.
 */
public class SongViewHolder {
    public LinearLayout songLableParent;
    public TextView songLabel;
    public ImageView songIcon;
    public ImageView songDeleteIcon;
    public TextView songStreamType;
    public boolean needInflate = false;
}
