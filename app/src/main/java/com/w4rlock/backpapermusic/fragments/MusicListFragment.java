package com.w4rlock.backpapermusic.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.w4rlock.backpapermusic.MusicPlayListActivity;
import com.w4rlock.backpapermusic.MusicPlayerActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.MusicGridViewAdapter;
import com.w4rlock.backpapermusic.adapters.MusicListViewAdapter;
import com.w4rlock.backpapermusic.adapters.MusicRecyclerAdapter;
import com.w4rlock.backpapermusic.controllers.GridViewSizeController;
import com.w4rlock.backpapermusic.controllers.PlayListController;
import com.w4rlock.backpapermusic.loaders.MusicDataLoader;
import com.w4rlock.backpapermusic.models.AsyncResult;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;
import com.w4rlock.backpapermusic.views.SongDialogPopup;

import java.util.ArrayList;
import java.util.List;

public class MusicListFragment extends Fragment {
    private static final String DEBUG_TAG = MusicListFragment.class.toString();
    public static final ArrayList<Song> musics = new ArrayList<Song>();
    private BaseAdapter mBaseAdapter;

    private ListView songsList;
    private ViewFlipper mViewFlipper;
    private GridView songsGridView;
    private RecyclerView mMusicRecyclerView;
    private RecyclerView.Adapter mMusicRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager layoutManager;
    private int mGridViewColumnWidth;
    final MusicLoaderCallbacks musicLoaderCallbacks = new MusicLoaderCallbacks();
    private static final int LOADER_ID = 0;
    public MusicListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music_list, container, false);
        setHasOptionsMenu(true);
        songsList = (ListView) rootView.findViewById(R.id.songs_list_view);
        songsGridView = (GridView) rootView.findViewById(R.id.songs_grid_view);
        mMusicRecyclerView = (RecyclerView) rootView.findViewById(R.id.songs_recycler_view);
        mMusicRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mMusicRecyclerView.setLayoutManager(layoutManager);
//        mMusicRecyclerView.setLayoutManager(mLayoutManager);

        mViewFlipper = (ViewFlipper) rootView.findViewById(R.id.view_flipper);
        songsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song music = (Song) parent.getItemAtPosition(position);
                showDialogWithSongs(music);
            }
        });

        songsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song music = (Song) parent.getItemAtPosition(position);
                showDialogWithSongs(music);
            }
        });
        mGridViewColumnWidth = GridViewSizeController.fixGridViewColumnWidth(getActivity().getApplicationContext(),songsGridView);
                (rootView.findViewById(R.id.stopService)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MusicService.ACTION_QUIT);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        (rootView.findViewById(R.id.showPlayer)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent playerIntent = new Intent(getActivity(), MusicPlayerActivity.class);
                playerIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().startActivity(playerIntent);
            }
        });
        (rootView.findViewById(R.id.showPlaylist)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent playerIntent = new Intent(getActivity(), MusicPlayListActivity.class);
                playerIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().startActivity(playerIntent);
            }
        });

        (rootView.findViewById(R.id.listview_icon)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                viewFlipperShowThisChild(0);
            }
        });

        (rootView.findViewById(R.id.gridview_icon)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                viewFlipperShowThisChild(1);
            }
        });

        (rootView.findViewById(R.id.recycler_view)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                viewFlipperShowThisChild(2);
            }
        });
        viewFlipperShowThisChild(2);
        loadData();
        return rootView;
    }

    private void viewFlipperShowThisChild(int whichChild) {
        switch (whichChild) {
            case 0:
                // show list view
                songsGridView.setAdapter(null);
                mBaseAdapter = null;
                mBaseAdapter = new MusicListViewAdapter(getActivity(), musics);
                songsList.setAdapter(mBaseAdapter);
                break;
            case 1:
                // show grid view
                songsList.setAdapter(null);
                mBaseAdapter = null;
                mBaseAdapter = new MusicGridViewAdapter(getActivity(), musics,mGridViewColumnWidth);
                Resources r = getContext().getResources();
                int noOfColumns = r.getInteger(R.integer.gridview_no_of_cols);
                songsGridView.setNumColumns(noOfColumns);
                songsGridView.setAdapter(mBaseAdapter);
                break;
            case 2:
                // show songs_recycler_view
                if(mMusicRecyclerAdapter == null) {
                    mMusicRecyclerAdapter = new MusicRecyclerAdapter(musics, getActivity(),mRecyclerViewSongSelected);
                    mMusicRecyclerView.setAdapter(mMusicRecyclerAdapter);
                }
                break;
            default:
                break;
        }
        mViewFlipper.setDisplayedChild(whichChild);
    }

    private void loadData() {
        getLoaderManager().initLoader(LOADER_ID, null, musicLoaderCallbacks);
    }

    public void forceLoadDataFromLoader() {
        L.d(DEBUG_TAG, "force load called ");
        try {
            getLoaderManager().restartLoader(0, null, musicLoaderCallbacks);
        } catch (Exception e) {
            L.d(DEBUG_TAG, "force load called but unable to fulfill it so failing silently");
        }

    }

    final SongDialogPopup.OnSongItemClicked onSongItemClickListener = new SongDialogPopup.OnSongItemClicked() {
        @Override
        public void onSongItemClicked(ArrayList<SongItem> songItems) {
            addToPlayList(songItems);
        }

        @Override
        public void downloadSongItemsClicked(ArrayList<SongItem> songItems) {
            Utils.downloadTheseSongs(songItems,DEBUG_TAG,getActivity());
        }
    };

    protected void showDialogWithSongs(Song song) {
        SongDialogPopup songDialogPopup = new SongDialogPopup(getActivity(), song, onSongItemClickListener);
        songDialogPopup.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        songDialogPopup.show();
    }

    protected void addToPlayList(ArrayList<SongItem> songItems) {
        PlayListController.addToPlayList(songItems,getActivity());
        Intent i = new Intent(MusicService.ACTION_PLAY);
        i.setPackage(MusicService.ACTION_PACKAGE_NAME);
        getActivity().startService(i);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.music, menu);
        SearchView searchView;
        if (android.os.Build.VERSION.SDK_INT >= 11)
            searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        else
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                L.d(DEBUG_TAG, "query: " + newText);
                filterMusicItems(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
    }

    public void filterMusicItems(String newText) {
        if (newText == null || newText.length() <= 0) {
            if (mViewFlipper.getDisplayedChild() == 0)
                mBaseAdapter = new MusicListViewAdapter(getActivity(), musics);
            else if (mViewFlipper.getDisplayedChild() == 1)
                mBaseAdapter = new MusicGridViewAdapter(getActivity(), musics,mGridViewColumnWidth);
            else if(mViewFlipper.getDisplayedChild() == 2){
                mMusicRecyclerAdapter = new MusicRecyclerAdapter(musics, getActivity(),mRecyclerViewSongSelected);
                mMusicRecyclerView.setAdapter(mMusicRecyclerAdapter);
            }
        } else {
            List<Song> tempMusic = new ArrayList<Song>();
            for (Song m : musics) {
                if (m.getAlbumName().toLowerCase().contains(newText) || m.getDirname().toLowerCase().contains(newText)) {
                    tempMusic.add(m);
                }
            }
            if (mViewFlipper.getDisplayedChild() == 0)
                mBaseAdapter = new MusicListViewAdapter(getActivity(), tempMusic);
            else if (mViewFlipper.getDisplayedChild() == 1)
                mBaseAdapter = new MusicGridViewAdapter(getActivity(), tempMusic,mGridViewColumnWidth);
            else if(mViewFlipper.getDisplayedChild() == 2){
                mMusicRecyclerAdapter = new MusicRecyclerAdapter(tempMusic, getActivity(),mRecyclerViewSongSelected);
                mMusicRecyclerView.setAdapter(mMusicRecyclerAdapter);
            }
        }
        if (mViewFlipper.getDisplayedChild() == 0)
            songsList.setAdapter(mBaseAdapter);
        else if (mViewFlipper.getDisplayedChild() == 1)
            songsGridView.setAdapter(mBaseAdapter);
        if(mBaseAdapter != null)
            mBaseAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private class MusicLoaderCallbacks implements LoaderManager.LoaderCallbacks<AsyncResult<ArrayList<Song>>> {

        @Override
        public Loader<AsyncResult<ArrayList<Song>>> onCreateLoader(int id, Bundle args) {
            return new MusicDataLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<AsyncResult<ArrayList<Song>>> loader, AsyncResult<ArrayList<Song>> data) {
            if (data.getResult() == null || data.isError()) {
                Toast.makeText(getActivity(), "No Data Found locally and unable to fetch data online",
                        Toast.LENGTH_LONG).show();
                return;
            }
            L.d(DEBUG_TAG+" MusicLoaderCallbacks", data.getResult().size() + "");
            if (data.isError()) {
                return;
            }
            musics.clear();
            for (Song m : data.getResult()) {
                musics.add(m);
            }
            if (mBaseAdapter != null)
                mBaseAdapter.notifyDataSetChanged();
            if(mViewFlipper.getDisplayedChild() == 2){
                mMusicRecyclerAdapter = new MusicRecyclerAdapter(musics, getActivity(),mRecyclerViewSongSelected);
                mMusicRecyclerView.setAdapter(mMusicRecyclerAdapter);
            }
        }

        @Override
        public void onLoaderReset(Loader<AsyncResult<ArrayList<Song>>> loader) {

        }

    }

    private MusicRecyclerAdapter.RecyclerViewSongSelected mRecyclerViewSongSelected = new MusicRecyclerAdapter.RecyclerViewSongSelected() {
        @Override
        public void onSongSelected(SongItem songItem) {
            Intent i = new Intent(MusicService.ACTION_PLAY_SONG_ITEM);
            i.putExtra("song",songItem);
            i.setPackage(MusicService.ACTION_PACKAGE_NAME);
            getActivity().startService(i);
        }

        @Override
        public void onSongsSelected(List<SongItem> songItems) {

        }
    };
}