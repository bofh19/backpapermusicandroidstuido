package com.w4rlock.backpapermusic.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.w4rlock.backpapermusic.MusicPlayerActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.MusicListDisplayAdapter;
import com.w4rlock.backpapermusic.loaders.MusicDataLoader;
import com.w4rlock.backpapermusic.models.AsyncResult;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.util.L;

import java.util.ArrayList;

public class MusicListLoaderFragment extends Fragment {

    private static final String DEBUG_TAG = MusicListLoaderFragment.class.toString();
    public static final ArrayList<Song> music = new ArrayList<Song>();
    private MusicListDisplayAdapter mMusicAdapter;
    private static final int NETWORK_MAX_TRIES = 2;
    private int networkTries = 0;

    public MusicListLoaderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music, container, false);
        ExpandableListView songsList = (ExpandableListView) rootView.findViewById(R.id.songs_list_view);
        mMusicAdapter = new MusicListDisplayAdapter(getActivity(), music);
        songsList.setAdapter(mMusicAdapter);
        getLoaderManager().initLoader(0, null, new MusicLoaderCallbacks());
        songsList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String CURRENT_SONG = music.get(groupPosition).getSongPath(childPosition);
                playThisSong(groupPosition, childPosition);
                L.d(DEBUG_TAG, CURRENT_SONG);
                return true;
            }
        });
        ((Button) rootView.findViewById(R.id.stopService)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(MusicService.ACTION_PAUSE);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        ((Button) rootView.findViewById(R.id.showPlayer)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent playerIntent = new Intent(getActivity(), MusicPlayerActivity.class);
                playerIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().startActivity(playerIntent);
            }
        });
        return rootView;
    }

    protected void playThisSong(int groupPosition, int childPosition) {
        // SongItem songItem = new SongItem(music.get(groupPosition),
        // childPosition);
        // String songUrl = music.get(groupPosition).getSongPath(childPosition);
        // Intent i = new Intent(MusicService.ACTION_URL);
        // i.putExtra("song", songItem);
        // Uri uri = Uri.parse(songUrl);
        // i.setData(uri);
        // getActivity().startService(i);
    }

    private class MusicLoaderCallbacks implements LoaderManager.LoaderCallbacks<AsyncResult<ArrayList<Song>>> {

        @Override
        public Loader<AsyncResult<ArrayList<Song>>> onCreateLoader(int id, Bundle args) {
            return new MusicDataLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<AsyncResult<ArrayList<Song>>> loader, AsyncResult<ArrayList<Song>> data) {
            networkTries += 1;
            if (data.getResult() == null) {
                if (networkTries <= NETWORK_MAX_TRIES) {
                    L.d(DEBUG_TAG+" MusicLoaderCallbacks", "null data retrying");
                    loader.reset();
                    getLoaderManager().restartLoader(loader.getId(), null, this);
                    return;
                }
                Toast.makeText(getActivity(), "Tries Exceeded Please Try Later", Toast.LENGTH_LONG).show();
                getActivity().finish();
                return;
            }
            L.d(DEBUG_TAG+" MusicLoaderCallbacks", data.getResult().size() + "");
            if (data.isError()) {
                return;
            }
            music.clear();
            networkTries = 0;
            for (Song m : data.getResult()) {
                music.add(m);
            }
            mMusicAdapter.notifyDataSetChanged();
        }

        @Override
        public void onLoaderReset(Loader<AsyncResult<ArrayList<Song>>> loader) {

        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}