package com.w4rlock.backpapermusic.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.PlayListDisplayAdapter;
import com.w4rlock.backpapermusic.controllers.PlayListController;
import com.w4rlock.backpapermusic.controllers.playlistToSong.SongViewHolder;
import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.models.PlayListItem;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

import java.util.ArrayList;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MusicPlayListFragment extends Fragment {
    private static final String DEBUG_TAG = MusicPlayListFragment.class.toString();
    public MusicPlayListFragment() {
    }

    PlayListDisplayAdapter playListDisplayAdapter;
    ListView listView;
    final ArrayList<PlayListItem> playListItems = new ArrayList<>();
    private boolean downloadPermissionGranted = false;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 12132;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music_play_list, container, false);
        setHasOptionsMenu(true);
        listView = (ListView) rootView.findViewById(R.id.musicPlaylistListView);
        playListDisplayAdapter = new PlayListDisplayAdapter(getActivity(),
                PlayListController.getFullPlayList(getActivity()), PlayListController.getCurrentPlayingItem(getActivity()), mPlayListCallback);
        listView.setAdapter(playListDisplayAdapter);
        playListItems.clear();
        playListItems.addAll(PlayListController.getFullPlayList(getActivity()));
        playListDisplayAdapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlayListItem playListItem = (PlayListItem) parent.getAdapter().getItem(position);
                SongItem songItem = PlayListController.getSongItemFromPlayListItem(playListItem, getActivity());
                Intent i = new Intent(MusicService.ACTION_PLAYLIST_PLAY);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                i.putExtra("song", songItem);
                getActivity().startService(i);
                PlayListController.playingThisSong(playListItem, getActivity());
                playListDisplayAdapter.setCurrentPlayListItem(playListItem);
                playListItems.clear();
                playListItems.addAll(PlayListController.getFullPlayList(getActivity()));
                playListDisplayAdapter.notifyDataSetChanged();
            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.music_play_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_play_list_edit:
                if (playListDisplayAdapter != null) {
                    playListDisplayAdapter.setDisplayRemoveButton(!playListDisplayAdapter.isDisplayRemoveButton());
                    playListDisplayAdapter.notifyDataSetChanged();
                }
                return true;
            case R.id.action_menu_play_list_download:
                downloadPlayListSongs();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    downloadPermissionGranted = true;
                    downloadPlayListSongs();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void downloadPlayListSongs() {

        class StartDownloadingPlayListItems extends AsyncTask<Void,Void,ArrayList<SongItem>>{
            Activity activity;
            ArrayList<PlayListItem> playListItems;
            ProgressDialog progressDialog;
            public StartDownloadingPlayListItems(Activity activity,ArrayList<PlayListItem> playListItems){
                this.activity = activity;
                this.playListItems = playListItems;
                progressDialog = new ProgressDialog(activity);
                progressDialog.setCancelable(false);
                progressDialog.setMessage(getString(R.string.loading));
            }

            @Override
            protected void onPostExecute(ArrayList<SongItem> songItems) {
                Utils.downloadTheseSongs(songItems,DEBUG_TAG,activity);
                progressDialog.dismiss();
            }

            @Override
            protected void onPreExecute() {
                progressDialog.show();
            }

            @Override
            protected ArrayList<SongItem> doInBackground(Void... params) {
                ArrayList<SongItem> songItems = new ArrayList<>();
                SQLiteDatabase db = MusicDb.getReadDatabase(activity);
                for (PlayListItem playListItem : playListItems) {
                    Song song = cupboard().withDatabase(db).query(Song.class).withSelection("hash_key = ?", "" + playListItem.hashKey).get();
                    songItems.add(new SongItem(song));
                }
                return songItems;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setPositiveButton(getActivity().getString(R.string.okay), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        StartDownloadingPlayListItems downloadingPlayListItems = new StartDownloadingPlayListItems(getActivity(),playListItems);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            int permissionCheck = getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            if(permissionCheck != PackageManager.PERMISSION_GRANTED){
                                downloadPermissionGranted = false;
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                            }else{
                                downloadPermissionGranted = true;
                            }
                        }else{
                            downloadPermissionGranted = true;
                        }
                        downloadingPlayListItems.execute();
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel),new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setTitle(getActivity().getString(R.string.playlist_download_confirmation))
                .setIcon(getActivity().getResources().getDrawable(R.drawable.ic_launcher))
                .setMessage(String.format(getActivity().getString(R.string.playlist_download_message),playListItems.size()));
        builder.show();
    }

    PlayListDisplayAdapter.PlayListDisplayAdapterCallback mPlayListCallback = new PlayListDisplayAdapter.PlayListDisplayAdapterCallback() {
        @Override
        public void onDelete(View view, int position) {
            L.d(DEBUG_TAG,"playListItemsCount: "+playListItems.size());
            PlayListItem playListItem = playListItems.get(position);
            PlayListController.removeFromPlayList(playListItem,getActivity());
            playListItems.clear();
            playListItems.addAll(PlayListController.getFullPlayList(getActivity()));
            playListDisplayAdapter.clear();
            for(PlayListItem playListItem1:playListItems){
                playListDisplayAdapter.add(playListItem1);
            }
            L.d(DEBUG_TAG,"playListItemsCount: "+playListItems.size());
            deleteCell(view, position);
        }
    };

    private void deleteCell(final View v, final int index) {
        Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                SongViewHolder vh = (SongViewHolder) v.getTag();
                vh.needInflate = true;
                L.d(DEBUG_TAG,"calling notify data set changed");
                playListDisplayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationStart(Animation animation) { }
        };
        collapse(v, al);
    }

    static final int ANIMATION_DURATION = 200;

    private void collapse(final View v, Animation.AnimationListener al) {
        final int initialHeight = v.getMeasuredHeight();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if (al != null) {
            anim.setAnimationListener(al);
        }
        anim.setDuration(ANIMATION_DURATION);
        v.startAnimation(anim);
    }

}
