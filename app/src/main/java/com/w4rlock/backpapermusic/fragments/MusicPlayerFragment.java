package com.w4rlock.backpapermusic.fragments;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.w4rlock.backpapermusic.MusicPlayListActivity;
import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.binder.MusicServiceBinder;
import com.w4rlock.backpapermusic.service.binder.MusicServiceCallbackInterface;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.service.google.MusicService.State;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

public class MusicPlayerFragment extends Fragment implements MusicServiceCallbackInterface {
    private static final String DEBUG_TAG = MusicPlayerFragment.class.toString();
    @Override
    public void onStart() {
        super.onStart();
//        attachToService();
    }

    @Override
    public void onResume() {
        L.d(DEBUG_TAG, "onResume");
        super.onResume();
        attachToService();
    }

    @Override
    public void onPause() {
        L.d(DEBUG_TAG, "onPause");
        super.onPause();
        detachFromService();
    }

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isMusicServiceRunning(getActivity()))
        // binding only if music
        // service is already
        // running
        {
            Intent intent = new Intent(getActivity(), MusicService.class);
            getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        } else
        // we set mSongItem to null and call
        // write song details
        // to set default values
        {
            mSongItem = null;
            writeSongDetails(false);
        }
    }

    private void detachFromService() {
        // Unbind from the service
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
        handler.removeCallbacks(onEverySecond);
        detachMusicPlayerCallbacks();
        mMusicService = null;
    }

    @Override
    public void onStop() {
        super.onStop();
//        detachFromService();
    }

    MusicService mMusicService;
    boolean mBound = false;
    private SeekBar mSeekBarPlayer;
    private TextView mMediaTime;
    private MediaPlayer mPlayer;
    private SongItem mSongItem;
    private TextView mSongDetails;
    private ImageView mSongThumb;
    private TextView mMediaPercent;
    private ViewGroup mainLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music_player, container, false);
        setHasOptionsMenu(true);
        mSongDetails = (TextView) rootView.findViewById(R.id.songDetails);
        mSeekBarPlayer = (SeekBar) rootView.findViewById(R.id.playerSeekbar);
        mMediaTime = (TextView) rootView.findViewById(R.id.mediaTime);
        mSongThumb = (ImageView) rootView.findViewById(R.id.songThumb);
        mMediaPercent = (TextView) rootView.findViewById(R.id.mediaPercent);
        mainLayout = (ViewGroup) rootView.findViewById(R.id.music_player_main_layout);
        mSeekBarPlayer.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progress;
            boolean fromUser;

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                L.d(DEBUG_TAG, "onStopTrackingTouch");
                if (fromUser && mPlayer.isPlaying()) {
                    mPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                L.d(DEBUG_TAG, "onStartTrackingTouch");
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.fromUser = fromUser;
                this.progress = progress;
            }
        });

        rootView.findViewById(R.id.showPlayList).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), MusicPlayListActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i);
            }
        });

        ((Button) rootView.findViewById(R.id.pauseButton)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_PAUSE);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });

        ((Button) rootView.findViewById(R.id.nextButton)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_SKIP);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        ((Button) rootView.findViewById(R.id.previousButton)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_REWIND);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });
        ((Button) rootView.findViewById(R.id.playButton)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                detachFromService();
                Intent i = new Intent(MusicService.ACTION_PLAY);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
                attachToService();

            }
        });

        ((Button) rootView.findViewById(R.id.stopButton)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MusicService.ACTION_STOP);
                i.setPackage(MusicService.ACTION_PACKAGE_NAME);
                getActivity().startService(i);
            }
        });

        writeSongDetails(false);
        return rootView;
    }

    private void writeSongDetails(SongItem songItem, boolean onPreperaing) {
        mSongItem = songItem;
        writeSongDetails(onPreperaing);
    }

    private void writeSongDetails(boolean onPreparing) {
        if (mSongItem != null) {
            mSongDetails.setText(mSongItem.getTitle() + " - " + mSongItem.getAlbum() + " - " + mSongItem.getArtist()
                    + (onPreparing ? " (Loading)" : " (Playing)"));
            //GetThumbnailDrawable getThumbnailDrawable = new GetThumbnailDrawable(mSongItem);
            // getThumbnailDrawable.execute();
        } else {
            mSeekBarPlayer.setMax(0);
            mSeekBarPlayer.setProgress(0);
            mSongDetails.setText(getActivity().getString(R.string.playing_nothing));
            mMediaTime.setText(getActivity().getString(R.string.time_duration_as_zeros));
            mSongThumb.setImageResource(R.drawable.dummy_album_art);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.music_player, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_player_share:
                showWithPlayingSongItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showWithPlayingSongItem() {
        if (mSongItem == null) {
            Utils.displayShortToast(getString(R.string.share_error_null), getActivity());
            return;
        }
//        Bitmap bitmap = ((BitmapDrawable) mSongThumb.getDrawable()).getBitmap();
//        if (bitmap != null) {
//        String pathOfBmp = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "title", null);
//        Uri bmpUri = Uri.parse(pathOfBmp);
//        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "asdfsdfsdf");
//        shareIntent.setType("image/png");
//        getActivity().startActivity(shareIntent);
//        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, mSongItem.getShareIntentString());
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

    protected void attachMusicPlayerCallbacks() {
        mMusicService.unregisterCallbackListener(ApplicationProperties.ServiceControlKey.musicPlayerServiceKey,MusicPlayerFragment.this);
        mMusicService.registerCallbackListener(ApplicationProperties.ServiceControlKey.musicPlayerServiceKey,MusicPlayerFragment.this);
    }

    protected void detachMusicPlayerCallbacks() {
        if (mMusicService != null)
            mMusicService.unregisterCallbackListener(ApplicationProperties.ServiceControlKey.musicPlayerServiceKey,MusicPlayerFragment.this);
        mPlayer = null;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            detachMusicPlayerCallbacks();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            MusicServiceBinder binder = (MusicServiceBinder) service;
            mMusicService = binder.getService();
            mBound = true;
            attachMusicPlayerCallbacks();
        }
    };

    @Override
    public void onSongChanged(SongItem songItem) {
        L.d(DEBUG_TAG, "onSongChanged");
    }

    @Override
    public void onStateChanged(State state) {
        L.d(DEBUG_TAG, "onStateChanged: " + state.toString());
    }

    @Override
    public void onPause(SongItem songItem) {
        L.d(DEBUG_TAG, "onPause SongItem");
        super.onPause();
    }

    @Override
    public void onPlay(SongItem songItem, MediaPlayer mPlayer) {
        L.d(DEBUG_TAG, "onPlay");
        mSongItem = songItem;
        this.mPlayer = mPlayer;
        writeSongDetails(false);
        handler.postDelayed(onEverySecond, 1000);
        mMediaPercent.setText("");
    }

    @Override
    public void onMediaPlayerStop() {
        L.d(DEBUG_TAG, "onMediaPlayerStop");
        mSongItem = null;
        handler.removeCallbacks(onEverySecond);
        writeSongDetails(false);
    }

    @Override
    public void onSongCompletion() {
        L.d(DEBUG_TAG, "onSongCompletion");
    }

    @Override
    public void onPrepared(MediaPlayer mPlayer) {
        L.d(DEBUG_TAG, "onPrepared");
    }

    @Override
    public void onError(MediaPlayer mp, int what, int extra) {
        L.d(DEBUG_TAG, "onError");
    }

    @Override
    public void onMediaPlayerReleased() {
        L.d(DEBUG_TAG, "onMediaPlayerReleased");
    }

    @Override
    public void onMediaPlayerCreated() {
        L.d(DEBUG_TAG, "onMediaPlayerCreated");
    }

    @Override
    public void onMostCommonColorAcquired(String hexColor) {
        L.d(DEBUG_TAG, "onMostCommonColorAcquired " + hexColor);
        if (hexColor != null) {
            try {
                mainLayout.setBackgroundColor(Color.parseColor(hexColor));
                android.support.v7.app.ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(hexColor)));

            } catch (Exception e) {
                L.d(DEBUG_TAG, "unable to set most common color: " + e.toString());
            }
        }
    }

    @Override
    public void onThumbnailDrawableAcquired(Bitmap result) {
        L.d(DEBUG_TAG, "onThumbnailDrawableAcquired ");
        if (!isAdded()) {
            return;
        }
        if (result != null) {
            mSongThumb.setImageBitmap(result);
        } else {
            L.d(DEBUG_TAG, "onThumbnailDrawableAcquired Result is Null");
            mSongThumb.setImageResource(R.drawable.dummy_album_art);
        }
    }

    @Override
    public void onPreparing(SongItem songItem) {
        L.d(DEBUG_TAG, "onPreparing");
        writeSongDetails(songItem, true);
    }

    @Override
    public void onBufferUpdate(MediaPlayer mp, int percent) {
        L.d(DEBUG_TAG, "onBufferUpdate " + percent);
        mMediaPercent.setText(" - " + percent);
    }

    // updating seekbar and timers
    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            handler.removeCallbacks(onEverySecond);
            try {
                if (mPlayer != null && mPlayer.isPlaying()) {
                    duration = mPlayer.getDuration();
                    mSeekBarPlayer.setMax(duration);
                    mSeekBarPlayer.setProgress(mPlayer.getCurrentPosition());
                    updateTime();
                } else {
                    mSeekBarPlayer.setMax(0);
                    mSeekBarPlayer.setProgress(0);
                }
            } catch (Exception e) {
                L.d(DEBUG_TAG, "" + e.toString());
            }
            handler.postDelayed(onEverySecond, 1000);
        }
    };

    private int current = 0;
    private int duration = 0;
    private final Handler handler = new Handler();

    private void updateTime() {
        current = mPlayer.getCurrentPosition();
        // L.d(DEBUG_TAG, "duration - " + duration + " current- " + current);
        int dSeconds = (int) (duration / 1000) % 60;
        int dMinutes = (int) ((duration / (1000 * 60)) % 60);
        int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

        int cSeconds = (int) (current / 1000) % 60;
        int cMinutes = (int) ((current / (1000 * 60)) % 60);
        int cHours = (int) ((current / (1000 * 60 * 60)) % 24);

        if (dHours == 0) {
            mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
        } else {
            mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours,
                    dMinutes, dSeconds));
        }
    }
}
