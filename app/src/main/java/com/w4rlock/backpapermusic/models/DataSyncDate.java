package com.w4rlock.backpapermusic.models;

import android.content.Context;
import android.content.SharedPreferences;

public class DataSyncDate {
    private static final String preferenceSetting = "com.w4rlock.backpapermusic.datasync.syncdate.pref";
    private static final String preferenceSettingValue = "com.w4rlock.backpapermusic.datasync.syncdate.value";
    private String generated_date;
    private Integer songs_added;

    public String getGenerated_date() {
        return generated_date;
    }

    public Integer getSongs_added() {
        return songs_added;
    }

    public void setGenerated_date(String generated_date) {
        this.generated_date = generated_date;
    }

    public void setSongs_added(Integer songs_added) {
        this.songs_added = songs_added;
    }

    public static void saveToPreferences(Context context, String generatedDate) {
        SharedPreferences settings = context.getSharedPreferences(preferenceSetting, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(preferenceSettingValue, generatedDate);
        editor.commit();
    }

    public static String getLastGeneratedDate(Context context) {
        SharedPreferences settings = context.getSharedPreferences(preferenceSetting, 0);
        return settings.getString(preferenceSettingValue, "");
    }
}
