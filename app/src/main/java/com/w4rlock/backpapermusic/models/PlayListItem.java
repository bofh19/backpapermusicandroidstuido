package com.w4rlock.backpapermusic.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import com.w4rlock.backpapermusic.database.db.MusicDb;
import com.w4rlock.backpapermusic.util.L;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by saikrishna on 09/06/14.
 */
// playedCount will be -1 if its not known or if its a temp object
public class PlayListItem {
    private static final String DEBUG_TAG = PlayListItem.class.toString();
    public Long _id;
    public Integer hashKey;
    public Integer playedCount = 0;

    private static final String preferenceSetting = "com.w4rlock.backpapermusic.models.playlistItem.currentPlaying.pref";
    private static final String preferenceSettingValueId = "com.w4rlock.backpapermusic.models.playlistItem.currentPlaying.value";

    public static void saveToPreferences(Context context, PlayListItem currentPlayListItem) {
        SharedPreferences settings = context.getSharedPreferences(preferenceSetting, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(preferenceSettingValueId, currentPlayListItem._id);
        editor.commit();
    }

    public static PlayListItem getPlayingItemHashKey(Context context) {
        SharedPreferences settings = context.getSharedPreferences(preferenceSetting, 0);
        Long id = settings.getLong(preferenceSettingValueId,-1L);
        SQLiteDatabase db = MusicDb.getReadDatabase(context);
        try {
            return cupboard().withDatabase(db).get(PlayListItem.class, id);
        }finally {
            db.close();
        }
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof PlayListItem){
            PlayListItem pl = (PlayListItem) o;
            L.d(DEBUG_TAG,"Instance is right: comparing items "+pl+" / / "+this);
            if(pl._id.equals(this._id) && pl.hashKey.equals(this.hashKey)){
                L.d(DEBUG_TAG,"matched here "+pl+"//"+this);
                return true;
            }
        }else{
            L.d(DEBUG_TAG,"Not even instance matched  / / "+this);
        }
        return false;
    }

    @Override
    public String toString() {
        return "_id: "+this._id+" hashKey: "+this.hashKey;
    }
}
