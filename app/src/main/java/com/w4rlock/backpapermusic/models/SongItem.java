package com.w4rlock.backpapermusic.models;

import android.content.Context;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.util.ApplicationProperties;
import com.w4rlock.backpapermusic.util.L;

import java.io.File;
import java.io.Serializable;

public class SongItem implements Serializable {
    private static final String DEBUG_TAG = "SongItem";
    private static final long serialVersionUID = 1L;
    private Song song;
    public boolean isSelected = false;
    private boolean localCopyAvailable = false;
    private String localCopyLocation = null;

    public boolean isLocalCopyAvailable() {
        return localCopyAvailable;
    }

    public void setLocalCopyAvailable(boolean localCopyAvailable) {
        this.localCopyAvailable = localCopyAvailable;
    }

    public String getLocalCopyLocation() {
        return localCopyLocation;
    }

    public void setLocalCopyLocation(String localCopyLocation) {
        this.localCopyLocation = localCopyLocation;
    }

    public SongItem(Song song) {
        this.song = song;
        checkAndSetIfLocalCopyAvailable();
    }

    private void checkAndSetIfLocalCopyAvailable() {
        L.d(DEBUG_TAG, "Checking if local copy is available: ");
        String outFileName = this.song.getName();
        String finaleFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + ApplicationProperties.MusicFolderDirectory + "/" + this.song.getDirname() + "/" + outFileName;
        L.d(DEBUG_TAG, "Checking File Path " + finaleFilePath);
        File file = new File(finaleFilePath);
        if (!file.exists()) {
            localCopyAvailable = false;
            localCopyLocation = null;
            L.d(DEBUG_TAG, "local file is not available.");
            return;
        } else {
            L.d(DEBUG_TAG, "local file is available probably.setting them " + finaleFilePath);
            localCopyAvailable = true;
            localCopyLocation = finaleFilePath;
        }


    }

    public String getArtist() {
        return this.song.getArtist();
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public String getTitle() {
        return song.getSongName(0);
    }

    public String getAlbum() {
        return song.getAlbum();
    }

    public long getDuration() {
        try {
            Integer durSec = song.getTime_secs();
            long total = ((long) durSec) * 1000;
            return total;
        } catch (Exception e) {
            return 0;
        }
    }

    public String getThumbUrl() {
        return song.getThumbnail();
    }

    public String getSongPath() {
        if (this.localCopyAvailable) {
            return this.localCopyLocation;
        }
        return song.getSongPath();
    }

    public String getReadableSize() {
        return song.getReadableSize();
    }

    public String getReadableDuration() {
        long duration = getDuration();
        int dSeconds = (int) (duration / 1000) % 60;
        int dMinutes = (int) ((duration / (1000 * 60)) % 60);
        int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

        if (dHours == 0) {
            return String.format("%02d:%02d", dMinutes, dSeconds);
        } else {
            return (String.format("%02d:%02d:%02d", dHours, dMinutes, dSeconds));
        }
    }

    public Spanned getPlayListDisplayString() {
        return Html.fromHtml("<b>" + this.getTitle() + "</b>" + "<br />" +
                "<small>" + this.getAlbum() + "/" + this.getArtist() + "</small>" + "<br />" +
                "<small>" + this.getReadableDuration() + "</small>");
    }

    public Spanned getSongDialogListDisplayString() {
        return Html.fromHtml("<b>" + this.getTitle() + "</b>" + "<br />" +
                "<small>" + this.getAlbum() + "/" + this.getArtist() + "</small>" + "<br />" +
                "<small>" + this.getReadableDuration() + " " + this.getReadableSize() + "</small>");
    }

    public String getShareIntentString() {
        return "listening to " + this.getTitle() +
                " from " + this.getAlbum() + " by " + this.getArtist() +
                " (" + this.getReadableDuration() + ")";
    }

    public Long getRowId() {
        if (this.song != null) {
            return this.song.get_id();
        }
        return -1L;
    }

    public int getStreamType() {
        if (this.localCopyAvailable)
            return R.string.local;
        else
            return R.string.stream;
    }
}
