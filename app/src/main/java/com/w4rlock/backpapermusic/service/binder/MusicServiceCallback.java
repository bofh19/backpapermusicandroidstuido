package com.w4rlock.backpapermusic.service.binder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService;
import com.w4rlock.backpapermusic.util.GetSongThumbDrawable;
import com.w4rlock.backpapermusic.util.ImageColour;
import com.w4rlock.backpapermusic.util.L;

import java.util.WeakHashMap;

/**
 * Created by saikrishna on 18/06/14.
 */
public class MusicServiceCallback implements MusicServiceCallbackInterface {
    WeakHashMap<Long, MusicServiceCallbackInterface> mServiceCallbacks = new WeakHashMap<>();
    private static final String DEBUG_TAG = MusicServiceCallback.class.toString();
    //    private final WeakList mServiceCallbacks = new WeakList();
    private Context context;

    public MusicServiceCallback(Context context) {
        this.context = context;
    }

    public void addCallback(Long key, MusicServiceCallbackInterface serviceCallback) {
//        mServiceCallbacks.add(serviceCallback);
        mServiceCallbacks.put(key, serviceCallback);
    }

    public void removeCallback(Long key, MusicServiceCallbackInterface serviceCallbackInterface) {
        mServiceCallbacks.remove(key);
        L.d(DEBUG_TAG, "removeCallback left Refs: " + mServiceCallbacks.size());
    }

    @Override
    public void onSongChanged(SongItem songItem) {
        GetThumbnailDrawable getThumbnailDrawable = new GetThumbnailDrawable(songItem);
        getThumbnailDrawable.execute();
        L.d(DEBUG_TAG, "onSongChanged");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onSongChanged(songItem);
            }
        }
    }

    @Override
    public void onStateChanged(MusicService.State state) {
        L.d(DEBUG_TAG, "onStateChanged: " + state.toString());
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                ((MusicServiceCallbackInterface) mServiceCallbacks.get(i)).onStateChanged(state);
            }
        }
    }

    @Override
    public void onPause(SongItem songItem) {
        L.d(DEBUG_TAG, "onPause");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onPause(songItem);
            }
        }
    }

    @Override
    public void onPlay(SongItem songItem, MediaPlayer mPlayer) {
        GetThumbnailDrawable getThumbnailDrawable = new GetThumbnailDrawable(songItem);
        getThumbnailDrawable.execute();
        L.d(DEBUG_TAG, "onPlay");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onPlay(songItem, mPlayer);
            }
        }
    }

    @Override
    public void onMediaPlayerStop() {
        L.d(DEBUG_TAG, "onMediaPlayerStop");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onMediaPlayerStop();
            }
        }
    }

    @Override
    public void onSongCompletion() {
        L.d(DEBUG_TAG, "onSongCompletion");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onSongCompletion();
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mPlayer) {
        L.d(DEBUG_TAG, "onPrepared");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onPrepared(mPlayer);
            }
        }
    }

    @Override
    public void onError(MediaPlayer mp, int what, int extra) {
        L.d(DEBUG_TAG, "onError");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onError(mp, what, extra);
            }
        }
    }

    @Override
    public void onMediaPlayerReleased() {
        L.d(DEBUG_TAG, "onMediaPlayerReleased");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onMediaPlayerReleased();
            }
        }
    }

    @Override
    public void onMediaPlayerCreated() {
        L.d(DEBUG_TAG, "onMediaPlayerCreated");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onMediaPlayerCreated();
            }
        }
    }

    @Override
    public void onMostCommonColorAcquired(String hexColor) {
        L.d(DEBUG_TAG, "onMostCommonColorAcquired " + hexColor);
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onMostCommonColorAcquired(hexColor);
            }
        }
    }

    @Override
    public void onThumbnailDrawableAcquired(Bitmap result) {
        L.d(DEBUG_TAG, "onThumbnailDrawableAcquired ");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onThumbnailDrawableAcquired(result);
            }
        }
    }

    @Override
    public void onPreparing(SongItem songItem) {
        GetThumbnailDrawable getThumbnailDrawable = new GetThumbnailDrawable(songItem);
        getThumbnailDrawable.execute();
        L.d(DEBUG_TAG, "onPreparing");
        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onPreparing(songItem);
            }
        }
    }

    @Override
    public void onBufferUpdate(MediaPlayer mp, int percent) {
        L.d(DEBUG_TAG, "onBufferUpdate " + percent);

        for (Long i : mServiceCallbacks.keySet()) {
            if (mServiceCallbacks.get(i) != null) {
                mServiceCallbacks.get(i).onBufferUpdate(mp, percent);
            }
        }
    }


    // get thumbnail drawable from cache if available or online
    // first checks on local cache / then goes for file cache
    // local cache checks occur through SongInfo object using song row _id as unique key
    class GetThumbnailDrawable extends AsyncTask<String, Void, Bitmap> {
        SongItem songItem;

        public GetThumbnailDrawable(SongItem songItem) {
            this.songItem = songItem;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            L.d(DEBUG_TAG, "GetThumbnailDrawable onPostExecute");
            onThumbnailDrawableAcquired(result);
            if (result != null) {
                GetAndMostCommonColor getAndMostCommonColor = new GetAndMostCommonColor(result, songItem);
                getAndMostCommonColor.execute();
            } else {
                onMostCommonColorAcquired(null);
                L.d(DEBUG_TAG, "GetThumbnailDrawable onPostExecute Result is Null");
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            L.d(DEBUG_TAG, "GetThumbnailDrawable getting thumbnail " + songInfo);
            if (songInfo == null) {
                L.d(DEBUG_TAG, "song info is null creating it now ");
                songInfo = new SongInfo();
                songInfo.songId = songItem.getRowId();
                songInfo.songThumbnail = null;
            } else {
                L.d(DEBUG_TAG, "song info is not null " + songInfo.songId);
            }
            if (songInfo != null && songInfo.songId.equals(songItem.getRowId()) && songInfo.songThumbnail != null) {
                L.d(DEBUG_TAG, "GetThumbnailDrawable got thumbnail from song info ");
            } else {
                L.d(DEBUG_TAG, "GetThumbnailDrawable fetching from cache/online");
                songInfo.songId = songItem.getRowId();
                try {
                    boolean loadFromUrl = true;
                    if (songItem != null && songItem.getThumbUrl() != null) {
                        if(songItem.isLocalCopyAvailable()){
                            android.media.MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(songItem.getSongPath());
                            byte [] data = mmr.getEmbeddedPicture();
                            if(data != null)
                            {
                                songInfo.songThumbnail = BitmapFactory.decodeByteArray(data, 0, data.length);
                                loadFromUrl = false;
                            }
                        }
                        if(loadFromUrl)
                            songInfo.songThumbnail = GetSongThumbDrawable.thumbFromUrl(context, songItem.getThumbUrl());
                        // making song most common color to null to re calculate from new drawable
                        songInfo.songHexColor = null;
                    }

                } catch (Exception e) {
                    L.d(DEBUG_TAG, "GetThumbnailDrawable error thumbnail " + e.toString());
                    e.printStackTrace();
                }
            }
            return songInfo.songThumbnail;
        }
    }

    // assumes songinfo is already initialized. else fail's silently and will return null
    class GetAndMostCommonColor extends AsyncTask<Void, Void, String> {
        Bitmap bitmap;
        SongItem songItem;

        public GetAndMostCommonColor(Bitmap bitmap, SongItem songItem) {
            this.bitmap = bitmap;
            this.songItem = songItem;
        }

        @Override
        protected String doInBackground(Void... params) {
            if (getSongInfoHexColor() != null) {
                L.d(DEBUG_TAG, "GetAndMostCommonColor got song Hex Color from song info "+getSongInfoHexColor());
            } else {
                L.d(DEBUG_TAG, "GetAndMostCommonColor calculating song Hex Color from song drawable ");
                try {
                    ImageColour imageColour = new ImageColour(bitmap);
                    if(imageColour.returnColour() == null){
                        return null;
                    }
                    String returnColor = "#" + imageColour.returnColour();
                    L.d(DEBUG_TAG, "GetAndMostCommonColor COLOR IDENTIFIED " + returnColor);
                    setSongInfoHexColor(returnColor);
                    songInfo.songHexColor = returnColor;
                } catch (Exception e) {
                    L.d(DEBUG_TAG, "GetAndMostCommonColor failed to get return color " + e.toString());
                }
            }
            if (songInfo != null)
                return songInfo.songHexColor;
            else
                return null;
        }

        @Override
        protected void onPostExecute(String s) {
            L.d(DEBUG_TAG, "GetAndMostCommonColor onPostExecute " + (s !=null) );
            if( s != null) {
                onMostCommonColorAcquired(s);
            }
        }
    }

    static class SongInfo {
        public Long songId;
        public String songHexColor;
        public Bitmap songThumbnail;
    }

    private SongInfo songInfo;

    private void setSongInfoHexColor(String hexColor) {
        if (songInfo != null)
            this.songInfo.songHexColor = hexColor;
    }

    private String getSongInfoHexColor() {
        if (this.songInfo != null && this.songInfo.songHexColor != null) {
            return songInfo.songHexColor;
        }
        return null;
    }
}
