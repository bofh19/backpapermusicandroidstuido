package com.w4rlock.backpapermusic.util;

import android.content.Context;
import android.graphics.Typeface;

public class ApplicationProperties {
    private ApplicationProperties() {
    }

    public static final String COMMON_FONT_STYLE = "fonts/RobotoCondensed-Regular.ttf";
    public static final String COMMON_FONT_STYLE_BOLD = "fonts/RobotoCondensed-Bold.ttf";

    public static final String MusicFolderDirectory = "Music";

    private static String selectedBitRate = "DEFAULT";

    public static String getSelectedBitRate() {
        return selectedBitRate;
    }

    public static void setSelectedBitRate(String selectedBitRate) {
        ApplicationProperties.selectedBitRate = selectedBitRate;
    }

    public static Typeface getTypeface(Context ctx) {
        return Typeface.createFromAsset(ctx.getAssets(), COMMON_FONT_STYLE);
    }


    public static class ServiceControlKey {
        public static final String KeyArgumentName = "ServiceControlKey";

        public static final Long musicPlayerServiceKey = 1L;
        public static final Long musicActivityPlayerControlServiceKey = 2L;
        public static final Long musicPlayListControlServiceKey = 3L;
    }

    public static boolean ShowAlertIfOnMobileNetwork = true;
}
