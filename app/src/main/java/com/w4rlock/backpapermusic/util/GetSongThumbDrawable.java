package com.w4rlock.backpapermusic.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.w4rlock.backpapermusic.util.lazyload.FileCache;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoaderUtils;
import com.w4rlock.backpapermusic.util.lazyload.MemoryCache;

public class GetSongThumbDrawable {
    public static Bitmap thumbFromUrl(Context context, String imageUrl) {
        FileCache fileCache = new FileCache(context);
        return ImageLoaderUtils.getBitmap(imageUrl, fileCache, new MemoryCache(), false, true, null, null);
    }
}
