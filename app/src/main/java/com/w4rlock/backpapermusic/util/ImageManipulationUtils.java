package com.w4rlock.backpapermusic.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.util.TypedValue;

import java.io.IOException;

/**
 * Created by saikrishna on 11/06/14.
 */
public class ImageManipulationUtils {
    private static final String DEBUG_TAG = ImageManipulationUtils.class.toString();

    public static float getPxFromDp(int dp, Context context) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    // IMAGE LOGICS

    public static enum ScalingLogic {
        CROP, FIT
    }

    public static Bitmap decodeResourceFromImage(String path, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight,
                scalingLogic);
        Bitmap unscaledBitmap = BitmapFactory.decodeFile(path, options);
        return unscaledBitmap;
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                          ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return srcWidth / dstWidth;
            } else {
                return srcHeight / dstHeight;
            }
        } else {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return srcHeight / dstHeight;
            } else {
                return srcWidth / dstWidth;
            }
        }
    }

    public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight,
                                            ScalingLogic scalingLogic) {
        Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight,
                scalingLogic);
        Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight,
                scalingLogic);
        Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                        ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.CROP) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                final int srcRectWidth = (int) (srcHeight * dstAspect);
                final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
                return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);
            } else {
                final int srcRectHeight = (int) (srcWidth / dstAspect);
                final int scrRectTop = (int) (srcHeight - srcRectHeight) / 2;
                return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
            }
        } else {
            return new Rect(0, 0, srcWidth, srcHeight);
        }
    }

    public static Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                        ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return new Rect(0, 0, dstWidth, (int) (dstWidth / srcAspect));
            } else {
                return new Rect(0, 0, (int) (dstHeight * srcAspect), dstHeight);
            }
        } else {
            return new Rect(0, 0, dstWidth, dstHeight);
        }
    }

    public static Bitmap decodeFile(String filePath, int WIDTH, int HEIGHT) {
        try {
            Bitmap unscBitmap = decodeResourceFromImage(filePath, WIDTH, HEIGHT, ScalingLogic.CROP);
            Bitmap b = createScaledBitmap(unscBitmap, WIDTH, HEIGHT, ScalingLogic.CROP);
            unscBitmap.recycle();
            try {
                ExifInterface exif = new ExifInterface(filePath);
                Integer or = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                L.d(DEBUG_TAG, "or is " + or);
                Matrix matrix = new Matrix();
                if (or == 0 || or == ExifInterface.ORIENTATION_NORMAL)
                    return b;
                else if (or == 6)
                    matrix.postRotate(90);
                else if (or == ExifInterface.ORIENTATION_ROTATE_90)
                    matrix.postRotate(90);
                else if (or == ExifInterface.ORIENTATION_ROTATE_180)
                    matrix.postRotate(180);
                else if (or == ExifInterface.ORIENTATION_ROTATE_270)
                    matrix.postRotate(270);
                else
                    return b;
                Bitmap rotatedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                b.recycle();
                return rotatedBitmap;
            } catch (IOException e) {
                L.d(DEBUG_TAG, "failed to rotate " + e.toString());
            }
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
