package com.w4rlock.backpapermusic.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.service.google.MusicService;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Utils {
    public static final int CurrentApiVersion = Build.VERSION.SDK_INT;
    public static final int HoneyCombApiVersion = Build.VERSION_CODES.HONEYCOMB;

    public static boolean isOverHoneyComb() {
        return CurrentApiVersion >= HoneyCombApiVersion;
    }

    public static boolean canShowBigPictureStyleNotification() {
        return CurrentApiVersion >= 16;
    }

    public static boolean canHaveRemoteControl() {
        return CurrentApiVersion >= 14;
    }

    public static boolean isDownloadManagerAvailable() {
        return CurrentApiVersion >= 9;
    }

    public static void displayLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static boolean isServiceRunning(Context context, String serviceName) {
        boolean serviceRunning = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(50);
        Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
        while (i.hasNext()) {
            ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) i.next();

            if (runningServiceInfo.service.getClassName().equals(serviceName)) {
                serviceRunning = true;
            }
        }
        return serviceRunning;
    }

    public static boolean isMusicServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MusicService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("NewApi")
    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static void displayShortToast(String message, Activity activity) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void customAlert(Activity activity, String message) {
        AlertDialog dialog = new AlertDialog.Builder(activity).setMessage(message).setTitle("Alert").setPositiveButton(activity.getString(R.string.okay), null).setCancelable(false).show();

         /* Common Font Style For Alert Dialogue */

        TextView textView = (TextView) dialog.findViewById(android.R.id.message);
        Typeface face = Typeface.createFromAsset(activity.getAssets(), ApplicationProperties.COMMON_FONT_STYLE);
        textView.setTypeface(face);

        ((TextView) dialog.findViewById(activity.getResources().getIdentifier("alertTitle", "id", "android"))).setTypeface(face);

        ((Button) dialog.getButton(AlertDialog.BUTTON_POSITIVE)).setTypeface(face);
    }

    /// this method assumes write permission exists, if it doesn't it fails silently
    public static void downloadTheseSongs(ArrayList<SongItem> songItems, String DEBUG_TAG, Context context) {
        DownloadManager mDownloadManager = null;
        L.d(DEBUG_TAG, "download button called. adding files to download manager: " + songItems.size());
        boolean toastDisplayed = false;
        if (Build.VERSION.SDK_INT >= 9) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                int permissionCheck = context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permissionCheck != PackageManager.PERMISSION_GRANTED){
                    Utils.displayLongToast(context, "no permission exists,");
                    return;
                }
            }
            mDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            for (SongItem songItem : songItems) {
                String dirPath = (Environment.DIRECTORY_DOWNLOADS).toString() + "/" + ApplicationProperties.MusicFolderDirectory + "/" + songItem.getSong().getDirname();
                File dirFile = new File(dirPath);
                if (!dirFile.exists())
                    dirFile.mkdirs();
                String outFileName = songItem.getSong().getName();
                String finaleFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + ApplicationProperties.MusicFolderDirectory + "/" + songItem.getSong().getDirname() + "/" + outFileName;
                L.d(DEBUG_TAG, "Checking File Path " + finaleFilePath);
                File file = new File(finaleFilePath);
                if (!file.exists()) {
                    L.d(DEBUG_TAG, "File Does Not Exist ");
                    L.d(DEBUG_TAG, "Queuing " + songItem.getSongPath() + " @ " + dirFile + "/" + outFileName);
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(songItem.getSongPath()));
                    request.setTitle(outFileName);
                    request.setDescription(dirPath + "/" + outFileName);
                    request.setDestinationInExternalPublicDir(dirPath, outFileName);
                    long enqueueId = mDownloadManager.enqueue(request);
                    L.d(DEBUG_TAG, "enqueueId: " + enqueueId);
                    if (!toastDisplayed) {
                        toastDisplayed = true;
                        Utils.displayLongToast(context, String.format(context.getString(R.string.download_notify_on_toast), songItem.getSong().getDirname()));
                    }
                } else {
                    L.d(DEBUG_TAG, "Not Queuing ");
                }
            }
        } else {
            Utils.displayLongToast(context, context.getString(R.string.download_manager_error));
        }
    }
}
