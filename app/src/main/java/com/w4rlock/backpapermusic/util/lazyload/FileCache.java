package com.w4rlock.backpapermusic.util.lazyload;

import java.io.File;
import android.content.Context;

import com.w4rlock.backpapermusic.util.StorageUtils;

public class FileCache {
    
    private File cacheDir;
    
    public FileCache(Context context){
//        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
//            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
//                    "com.w4rlock.backpapermusic.location.albumcache");
//        else
            cacheDir = context.getDir("albumcache",Context.MODE_PRIVATE);
        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }
    
    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename=String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}