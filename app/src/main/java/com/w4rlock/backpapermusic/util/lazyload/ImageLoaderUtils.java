package com.w4rlock.backpapermusic.util.lazyload;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import com.w4rlock.backpapermusic.util.ImageManipulationUtils;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by saikrishna on 13/06/14.
 */
public class ImageLoaderUtils {
    private static final String DEBUG_TAG = ImageLoaderUtils.class.toString();
    public static Bitmap getBitmap(String url,FileCache fileCache,MemoryCache memoryCache,boolean getExactBitmapBasedOnParams,boolean returnImageAsIs,Integer mGivenWidth,Integer mGivenHeight){
        File f = fileCache.getFile(url);

        //from SD cache
        Bitmap b = decodeFile(f,getExactBitmapBasedOnParams,returnImageAsIs,mGivenWidth,mGivenHeight);
        if (b != null)
            return b;

        //from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = decodeFile(f,getExactBitmapBasedOnParams,returnImageAsIs,mGivenWidth,mGivenHeight);
            return bitmap;
        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    //decodes image and scales it to reduce memory consumption
    public static Bitmap decodeFile(File f,boolean getExactBitmapBasedOnParams,boolean returnImageAsIs,Integer mGivenWidth,Integer mGivenHeight){
        try {
            if (getExactBitmapBasedOnParams) {
                try {
                    L.d(DEBUG_TAG, "getExactBitmapBasedOnParams: got exact width and height, returning bitmap based on those");
                    File bitmapBasedOnWH = new File(f.getAbsolutePath()+mGivenHeight+mGivenWidth);
                    FileOutputStream out = null;
                    Bitmap bitmap = null;
                    try {
                        FileInputStream stream2 = new FileInputStream(bitmapBasedOnWH);
                        bitmap = BitmapFactory.decodeStream(stream2, null, null);
                        stream2.close();
                    }catch (FileNotFoundException e2){
                        L.d(DEBUG_TAG,"getExactBitmapBasedOnParams: Failed to decode bitmap based on width and height, creating it now "+ e2.toString());
                    }
                    if(bitmap == null){
                        L.d(DEBUG_TAG,"getExactBitmapBasedOnParams: Failed to decode bitmap based on width and height, creating it now ");
                        out = new FileOutputStream(bitmapBasedOnWH);
                        Bitmap b = ImageManipulationUtils.decodeFile(f.getAbsolutePath(), mGivenWidth, mGivenHeight);
                        b.compress(Bitmap.CompressFormat.JPEG, 50, out);
                        L.d(DEBUG_TAG, "getExactBitmapBasedOnParams:  Generation THUMBNAIL Sucess : " + bitmapBasedOnWH.getAbsolutePath());
                        out.close();
                        return b;
                    }else{
                        L.d(DEBUG_TAG, "getExactBitmapBasedOnParams:  FOUND THUMBNAIL : " + bitmapBasedOnWH.getAbsolutePath());
                        return bitmap;
                    }
                } catch (Exception e) {
                    L.d(DEBUG_TAG,"getExactBitmapBasedOnParams: Exception while decoding image from file "+e.toString());
                }
            }else
                L.d(DEBUG_TAG,"decodeFile going the ordinary way");
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            // if returnImageAsIs is set to true
            // then inSampleSize is set to nothing
            // to not scale image
            if(!returnImageAsIs){
                o2.inSampleSize = scale;
            }
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
