package com.w4rlock.backpapermusic.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.w4rlock.backpapermusic.R;
import com.w4rlock.backpapermusic.adapters.SongItemListDisplayAdapter;
import com.w4rlock.backpapermusic.loaders.LoadSongsList;
import com.w4rlock.backpapermusic.loaders.LoadingInterface;
import com.w4rlock.backpapermusic.models.Song;
import com.w4rlock.backpapermusic.models.SongItem;
import com.w4rlock.backpapermusic.util.L;
import com.w4rlock.backpapermusic.util.lazyload.ImageLoader;

import java.util.ArrayList;

public class SongDialogPopup extends Dialog {
    private ArrayList<SongItem> songs;
    private OnSongItemClicked mOnSongItemClicked;
    ImageView mHeadingIcon;
    TextView mHeadingText;
    ListView mListView;
    Button mAddButton;
    Button mDownloadButton;
    SongItemListDisplayAdapter mSongItemListDisplayAdapter;
    private String DEBUG_TAG = SongDialogPopup.class.toString();
    private Context context;
    public SongDialogPopup(Context context, ArrayList<SongItem> songs, OnSongItemClicked onSongItemClicked) {
        super(context);
        this.context = context;
        this.songs = songs;
        this.mOnSongItemClicked = onSongItemClicked;
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.song_dialog_popup);
        if (songs.size() > 0)
            DEBUG_TAG += " [" + songs.get(0).getAlbum() + "]";
        mSongItemListDisplayAdapter = new SongItemListDisplayAdapter(context, songs);

        mListView = (ListView) findViewById(R.id.alert_dialog_list);
        mHeadingIcon = (ImageView) findViewById(R.id.alert_dialog_heading_icon);
        mHeadingText = (TextView) findViewById(R.id.alert_dialog_heading);
        mAddButton = (Button) findViewById(R.id.alert_dialog_add);
        mDownloadButton = (Button) findViewById(R.id.alert_dialog_download);

        (findViewById(R.id.alert_dialog_cancel)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public SongDialogPopup(Context context,Song song,OnSongItemClicked onSongItemClicked){
        super(context);
        this.context = context;
        this.songs = new ArrayList<>();
        this.mOnSongItemClicked = onSongItemClicked;

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.song_dialog_popup);

        mSongItemListDisplayAdapter = new SongItemListDisplayAdapter(context, songs);
        this.song = song;
        mListView = (ListView) findViewById(R.id.alert_dialog_list);
        mHeadingIcon = (ImageView) findViewById(R.id.alert_dialog_heading_icon);
        mHeadingText = (TextView) findViewById(R.id.alert_dialog_heading);
        mAddButton = (Button) findViewById(R.id.alert_dialog_add);
        mDownloadButton = (Button) findViewById(R.id.alert_dialog_download);
        (findViewById(R.id.alert_dialog_cancel)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private Song song = null;
    private void loadSongItemsFromSong(Song song) {
        LoadingInterface loadingInterface = new LoadingInterface() {
            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingEnded() {

            }

            @Override
            public void onProgressUpdate(int percent) {

            }
        };
        LoadSongsList.LoadSongsListCallback callback = new LoadSongsList.LoadSongsListCallback() {

            @Override
            public void onLoadingFinished(ArrayList<Song> songs) {
                ArrayList<SongItem> songItems = new ArrayList<SongItem>();
                for (Song song : songs) {
                    L.d(DEBUG_TAG, song.getName() + "/" + song.getDirname());
                    SongItem songItem = new SongItem(song);
                    songItems.add(songItem);
                }
                SongDialogPopup.this.songs = songItems;
                executeShowItems();
                mListView.setAdapter(mSongItemListDisplayAdapter);
            }
        };
        LoadSongsList loadSongsList = new LoadSongsList(context, loadingInterface, callback);
        loadSongsList.execute(song);
    }

    private void executeShowItems(){
        if (this.songs.size() < 0)
            return;
        mSongItemListDisplayAdapter = new SongItemListDisplayAdapter(context, SongDialogPopup.this.songs);
        mListView.setAdapter(mSongItemListDisplayAdapter);
        if (this.songs.size() > 0) {
            mHeadingText.setText(songs.get(0).getAlbum());
            ImageLoader imageLoader = new ImageLoader(getContext());
            imageLoader.DisplayImage(songs.get(0).getThumbUrl(), mHeadingIcon);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SongItem songItem = (SongItem) parent.getItemAtPosition(position);
                songItem.isSelected = !songItem.isSelected;
                mSongItemListDisplayAdapter.notifyDataSetChanged();
            }
        });

        mAddButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ArrayList<SongItem> songItems = new ArrayList<SongItem>();
                for (int i = 0; i < mSongItemListDisplayAdapter.getCount(); i++) {
                    SongItem si = mSongItemListDisplayAdapter.getItem(i);
                    if (si.isSelected)
                        songItems.add(si);
                }
                L.d(DEBUG_TAG, "selected " + songItems.size());
                mOnSongItemClicked.onSongItemClicked(songItems);
                dismiss();
            }
        });

        mDownloadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ArrayList<SongItem> songItems = new ArrayList<SongItem>();
                for (int i = 0; i < mSongItemListDisplayAdapter.getCount(); i++) {
                    SongItem si = mSongItemListDisplayAdapter.getItem(i);
                    if (si.isSelected)
                        songItems.add(si);
                }
                L.d(DEBUG_TAG, "selected " + songItems.size());
                mOnSongItemClicked.downloadSongItemsClicked(songItems);
                dismiss();
            }
        });
    }
    @Override
    public void show() {
        L.d(DEBUG_TAG, "showing pop up");
        executeShowItems();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        super.show();
        super.getWindow().setAttributes(lp);
        if(song != null)
            loadSongItemsFromSong(song);
    }

    @Override
    public void dismiss() {
        mListView.setAdapter(null);
        mListView.setOnItemClickListener(null);
        mSongItemListDisplayAdapter = null;
        super.dismiss();
    }

    public static interface OnSongItemClicked {
        public void onSongItemClicked(ArrayList<SongItem> songItems);

        public void downloadSongItemsClicked(ArrayList<SongItem> songItems);
    }

}
